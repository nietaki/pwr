package coloring;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import bfs.Color;
import bfs.Vertex;

public class ColoringCombinerImpl {
  
  public Vertex combine(LongWritable key, Iterator<Text> vertices) throws IOException{
    long id = key.get();
    
    //set base values to be later modified by reduced vertices
    Vertex ret = new Vertex();
    ret.setId(id);
    ret.setColor(Color.WHITE);
    ret.setDistance(-1);
    
    while(vertices.hasNext()) {
      Vertex v = Vertex.fromIdAndString(id, vertices.next().toString());
      
      //we assign each real distance only once 
      if (v.getDistance() != -1) {//no need to overwrite with not reachable
        if (ret.getDistance() == -1 || v.getDistance() < ret.getDistance()) //any distance if unreachable or better if already reachable
          ret.setDistance(v.getDistance());
      }
      
      if (v.getColor().getColorId() > ret.getColor().getColorId())
        ret.setColor(v.getColor());
      
      //all non-empty should have the same neighbors set
      if (!v.getNeighbors().isEmpty())
        ret.setNeighbors(v.getNeighbors());
      
    }
    
    //FIXME uncomment the following
    if(ret.getColor() == Color.BLACK) {
      //to increase efficiency - let's not decode neighbors of the vertices we won't be processing anymore
//      ret.resetNeighbors();
    }
    
    return ret;
  }

}

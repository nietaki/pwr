package bfs;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Vertex {
  
  public final static int UNREACHABLE = -1;
  
  private long id;
  public Vertex(long id, int distance, Color color, Collection<Long> neighbors) {
    super();
    this.id = id;
    this.distance = distance;
    this.color = color;
    this.neighbors = new LinkedList<Long>(neighbors);
  }

  private int distance;
  private Color color;
  
  private Collection<Long> neighbors = new LinkedList<Long>();
  
  public Vertex() {super();}
  
  
  public Collection<Long> getNeighbors() {
    return neighbors;
  }
  public void setNeighbors(Collection<Long> neighbors) {
    this.neighbors = new LinkedList<Long>(neighbors);
  }
  public void resetNeighbors() {
    this.neighbors = new LinkedList<Long>();
  }
  
  
  
  public Color getColor() {
    return color;
  }
  public void setColor(Color color) {
    this.color = color;
  }
  
  public long getId() {
    return id;
  }
  public void setId(long id) {
    this.id = id;
  }
  
  public int getDistance() {
    return distance;
  }
  public void setDistance(int distance) {
    this.distance = distance;
  }
  
  /**
   * for serialization - serializes everything but the id
   */
  @Override
  public String toString() {
    List<String> l = new LinkedList<String>();
//    l.add(String.valueOf(this.id));
    l.add(String.valueOf(distance));
    l.add(this.color.toString());
    for(Long n: neighbors) {
      l.add(String.valueOf(n));
    }
    
    return Vertex.join(l, " ");
  }
  
  /**
   * 
   * @param s string for deserialization
   * @return deserialized Vertex
   * @throws IOException 
   */
  public static Vertex fromIdAndString(long id, String s) throws IOException {
    String[] tokens = s.split(" ");
    if (tokens.length < 2) {
      throw new IOException(s + "couldn't be split into vertex definition");
    }
    
    int distance = Integer.parseInt(tokens[0]);
    Color color = Color.fromString(tokens[1]);
    
    LinkedList<Long> neighbors = new LinkedList<Long>();
    for(int i = 2; i < tokens.length; i++) {
      neighbors.add(Long.parseLong(tokens[i]));
    }
    
    
    Vertex v = new Vertex(id, distance, color, neighbors);
    return v;
  }
  
  private static String join(Iterable<? extends CharSequence> s, String delimiter) {
    Iterator<? extends CharSequence> iter = s.iterator();
    if (!iter.hasNext()) return "";
    StringBuilder buffer = new StringBuilder(iter.next());
    while (iter.hasNext()) 
      buffer.append(delimiter).append(iter.next());
    return buffer.toString();
  }
}

PWiR zadanie 1 - Jacek Królikowski 
==================================

W celu minimalizacji liczby stanów Promelowych zamiast stosowania pojedynczego,
zawijającego się licznika i terminów w jego ujęciu dla każdego z procesów, 
stworzyłem tablicę malejących liczników, oddzielnych dla każdego z procesów 
typu "worker", na których odmierzany jest czas zarówno uśpienia, jak i czynności
pilnych. Rozpoczęcie odliczania czasu uzyskiwane jest przez ustawienie w danym
liczniku wartości T1 lub T2 a przy osiągnięciu przez licznik wartości 0
odpowiedni proces bądź licznik mogą zostać zablokowane (w zależności od 
realizowanego zadania).

Usypianie zostało zaimplementowane za pomocą funkcji typu inline, a działania
pilne za pomocą #define, w celu łatwiejszego parametryzowania działań za 
pomocą konkretnych operacji.

Rozwiązanie jest napisane dla wartości N=4, T1=2, T2=3, aczkolwiek działa też
dla innych sensownych.

Wszystkie podpunkty zostały zapisane w jednym pliku, weryfikacja 
poszczególnych podpunktów odbywa się za pomocą odpowiedniego make'a.

Jaki warunek ograniczający T1 i T2 gwarantuje wzajemne wykluczanie?
-------------------------------------------------------------------

T1 < T2

Podpunkt 1
----------

Uruchomienie:
$ make zad1

Podpunkt 1 został zrealizowany za pomocą formuły typu LTL mówiącej, że w
żadnym momencie w sekcji krytycznej nie przebywa więcej niż jeden proces.

Formuła ta pokazuje to dla kombinacji procesów 1 i 2, aczkolwiek, jako że
sytuacja z pozostałymi procesami jest w pełni analogiczna, pokazanie pozostałych
kombinacji nie jest konieczne.

Weryfikacja za pomocą spina nie znajduje kontrprzykładu na całej przestrzeni stanów.


Podpunkt 2
----------

Uruchomienie:
$ make zad2

Podpunkt 2 został zrealizowany za pomocą formuły typu LTL mówiącej, że 
jeśli proces 1 wykonał protokół wstępny, to wejdzie kiedyś do sekcji krytycznej.

Weryfikacja za pomocą spin pokazuje fałszywość tej tezy.

Podpunkt 3
----------

Uruchomienie:
$ make zad3

Do realizacji tego podpunktu został dodany (ograniczony z obu stron) licznik mierzący
ilość czasu od wykonania przez dowolny proces protokołu wstępnego do wejścia przez 
jakiś proces do sekcji krytycznej i ustawiona została asercja (pod postacią formuły LTL)
na maksymalną wartość tego licznika.

Żeby uniknąc błędów typu "off by one", asercja ma następującą postać:
waitingTime < T1 + T2 + 3

Jeśli licznik osiąga wartość T1 + T2 + 3, oznacza to też, że osiąga każdą mniejszą.

Warunek z podpunktu 3 nie jest spełniony.

Podpunkt 4
----------

Uruchomienie:
$make zad4

Do weryfikacji tego podpunktu został do programu dodany kanał przyjmujący numery 
procesów wykonujących protokół wstępny i weryfikujący te numery w sekcji krytycznych.

Warunek z podpunktu 4 nie jest spełniony.

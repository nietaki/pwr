package coloring;

import java.io.IOException;
import java.util.LinkedList;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.Mapper;

import bfs.Color;
import bfs.Vertex;

public class ColoringMapper extends MapReduceBase implements Mapper<LongWritable, Text, LongWritable, Text> {
  @Override
  public void configure(JobConf job) {
  }
  
  @Override
  public void map(LongWritable key, Text value,
      OutputCollector<LongWritable, Text> output, Reporter reporter)
      throws IOException {
    
    long id = key.get();
    Vertex v = Vertex.fromIdAndString(id, value.toString());
    
    switch(v.getColor()) {
    case BLACK:
    case WHITE:
      //nothing to do with the vertex in the mapper, we're just processing the grays
      output.collect(new LongWritable(id), new Text(v.toString()));
      break;
    case GRAY:
      //first, color all v's neighbors and give them correct distances
      //we don't know their neighbors, so we'll pretend they don't have any. The reducer will fix that later:
      //WHITE: (id), neighbors + GRAY: color, distance
      
      for(long n: v.getNeighbors()) {
        Vertex nv = new Vertex();
        nv.setId(n);
        nv.setColor(Color.GRAY);
        nv.setDistance(v.getDistance() + 1);
        
        output.collect(new LongWritable(n), new Text(nv.toString()));
      }
      
      //finally color v black
      v.setColor(Color.BLACK);
      output.collect(new LongWritable(id), new Text(v.toString()));
      break;
    default:
      // :o
    }
  }


}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>
#include "laplace-common.h"

/** CONSTANTS **/

#define TAG_INIT 13
#define TAG_ROW 14
#define TAG_FINISHED_CHECK 15
#define TAG_RESULTS 16 

#define OPTION_VERBOSE "--verbose"
//only decides debug output format
#define DEBUG 0
int REALP = 0;
int P = 0;
int N = 32;
/*
 * different for each process
 */

int myRank;
// index of the first row, not including additional rows
int startingRow = 0;
// row count for the current process, INCLUDING additional rows
int rowCount = 0;

//stride sizes WITHOUT the border rows
int normalStrideSize;
int lastStrideSize;

double *points = NULL; 

/*
 * some additional variables
 */

int numIterations;
double omega, epsilon;
struct timeval startTime;
int verbose = 0;


static void printUsage(char const * progName)
{
  if(myRank == 0)
    fprintf(stderr, "Usage:\n"
      "    %s [--verbose] <N>\n"
      "Where:\n"
      "   <N>         The number of points in each dimension (at least 2).\n"
      "   " OPTION_VERBOSE "   Prints the input and output systems.\n",
      progName); 
}

static void printLines(double *bufferBeginning, int lineCount) {
  if(verbose) {
    int i, j, off;
    off = 0;
    for(i = 0; i < lineCount; i++) {
      for(j = 0; j < N; j++) {
        if(j != 0)
          printf(" ");
        printf("%.10f", points[off]);
        off++;
      }
      printf("\n");
    }
  }
}

/*
 * faking 2d array access to points - quick and nice
 */
static double *pointsRow(int i) {
  return points + (i * N);
}

static struct timeval getTimeStructNow() {
  struct timeval ret;
  if(gettimeofday(&ret, NULL))
  {
    fprintf(stderr, "ERROR: Gettimeofday (start) failed!\n");
  }
  return ret;
}

static double getTimeSince(struct timeval then) {
  struct timeval untilTime;
  if(gettimeofday(&untilTime, NULL))
  {
    fprintf(stderr, "ERROR: Gettimeofday (end) failed!\n");
  }

  double duration =
    ((double)untilTime.tv_sec + ((double)untilTime.tv_usec / 1000000.0)) - 
    ((double)then.tv_sec + ((double)then.tv_usec / 1000000.0));

  return duration;
}

static void startStopwatch() { 
  startTime = getTimeStructNow();
}



static double stopwatchTime() {
  return getTimeSince(startTime);
}


/**
 * simply brilliant
 */
static double getGlobalMaxDiff(double localMaxDiff) {
  double globalMaxDiff;
  MPI_Allreduce(&localMaxDiff, &globalMaxDiff, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  return globalMaxDiff;
}

static void deadEnd() {
  

  double localMaxDiff, globalMaxDiff; 
  localMaxDiff = 0.0;
  do { 
    globalMaxDiff = getGlobalMaxDiff(localMaxDiff);
  } while (globalMaxDiff > epsilon); 

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}

static void results() {
  if (myRank != 0) {
    //sending just the needed rows, nothing more, nothing less
    int lineCount = (myRank == P - 1)? lastStrideSize : normalStrideSize;

    //printf("%d will send results\n", myRank);
    //always points + N, we are never dealing with 0
    MPI_Send(points + N, lineCount * N, MPI_DOUBLE, 0, TAG_RESULTS, MPI_COMM_WORLD); 
    //printf("%d sent results to the 0th process\n", myRank);
  } else {
    //P == 0
    //no whole buffer
    int stride = 0;
    printLines(points, rowCount - 1);

    for(stride = 1; stride < P; stride++) {
      //receive and print others
      int lineCount = (stride == P - 1)? lastStrideSize : normalStrideSize;
      
      MPI_Status status; 

      //printf("0 is waiting for results from %d\n", stride);
      int code = MPI_Recv(points, lineCount * N, MPI_DOUBLE, stride, TAG_RESULTS, MPI_COMM_WORLD, &status); 
      if(code == 0) {
        //printf("received results from %d no problem!\n", stride);
      } else {
        printf("got an error receiving results from %d with error code %d!\n", stride , code);
      }

      printLines(points, lineCount);
    }
  }
}
void myexit(int status) {
  MPI_Finalize();
  exit(status);
}

void parseArgs(int argc, char *argv[]) {
  if (argc < 2)
  {
    if(myRank == 0) fprintf(stderr, "ERROR: Too few arguments!\n");
    printUsage(argv[0]);
    myexit(1);
  }
  else if (argc > 3)
  {
    if(myRank == 0) fprintf(stderr, "ERROR: Too many arguments!\n");
    printUsage(argv[0]);
    myexit(1);
  }
  else
  {
    int argIdx = 1;
    if (argc == 3)
    {
      if (strncmp(argv[argIdx], OPTION_VERBOSE, strlen(OPTION_VERBOSE)) != 0)
      {
        if(myRank == 0) fprintf(stderr, "ERROR: Unexpected option '%s'!\n", argv[argIdx]);
        printUsage(argv[0]);
        myexit(1);
      }
      verbose = 1;
      ++argIdx;
    }
    N = atoi(argv[argIdx]);
    if (N < 2)
    {
      if(myRank == 0) fprintf(stderr, "ERROR: The number of points, '%s', should be "
          "a numeric value greater than or equal to 2!\n", argv[argIdx]);
      printUsage(argv[0]);
      myexit(1);
    }
  }
}
int main(int argc, char * argv[])
{
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &REALP);
  P = REALP;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  
  parseArgs(argc, argv);
//  if(myRank == 0)
//    printf("process 0 says  there are %d process altogether\n", P);
//  
//  printf("Hello world from %d!\n", myRank);
  P = (N < P)?N:P; 
  
  omega = getOmega(N);
  epsilon = getEpsilon(N);

  normalStrideSize = N / P;
  lastStrideSize = N - (normalStrideSize * P) + normalStrideSize;
 
  /* fixing strides to make them more effective */
  if (lastStrideSize > normalStrideSize) {
    normalStrideSize++;
    P = N / normalStrideSize;
    if (P * normalStrideSize < N) {
      P++;
      lastStrideSize = N - ((P - 1) * normalStrideSize);
    } else{
      lastStrideSize = normalStrideSize;
    }
    //now last stride is normal size or smaller
  }
 
  if(myRank >= P) {
    deadEnd();
    return 0;
  }


  int *startingRows = (int*) malloc(P * sizeof(int));
  int *rowCounts = (int*) malloc(P * sizeof(int));

  int i;
  for(i = 0; i < P; i++) {
    startingRows[i] = i * normalStrideSize;

    //base strideSize, without the neighboring rows
    if (i == P - 1) {
      rowCounts[i] = lastStrideSize;
    } else {
      rowCounts[i] = normalStrideSize;
    }

    //adjusting startingRows and strideSizes
    if (i == 0) {
      rowCounts[i] = rowCounts[i] + 1;
    } else if (i != P - 1) {
      startingRows[i] = startingRows[i] - 1;
      rowCounts[i] = rowCounts[i] + 2;
    } else {
      //last row
      startingRows[i] = startingRows[i] - 1;
      rowCounts[i] = rowCounts[i] + 1;
    }
  } 
   
  startingRow = startingRows[myRank];
  rowCount = rowCounts[myRank];

  //allocate enough memory to fit each stride with additional rows - gets reused in collecting results 
  points = (double*) malloc((normalStrideSize + 2) * N * sizeof(double)); 

  //main process-specific tasks
  if (myRank == 0) {
    //making sure there is enough space
    double *buffer = (double*) malloc(N * (normalStrideSize + 2) * sizeof(double));
    //                                                     ^- for corner cases
    
    //skipping the first stride 
    int stride;
    int off;
    int j;
    //FIXME: could be repraced with MPI_Scatterv
    for(stride = P - 1; stride >= 0; stride--) {
      off = 0;
      for(i = startingRows[stride]; i < startingRows[stride] + rowCounts[stride] ; i++) {
        for(j = 0 ; j < N; j++) {
	        if(stride > 0) {
	          //printf("off: %d", off);
            buffer[off] = getInitialValue(i, j, N);
	        } else { //stride == 0
	          points[off] = getInitialValue(i, j, N);
	        }
          off++;
        }
      }
      if(stride != 0) {
        int bufSize = rowCounts[stride] * N;
        MPI_Send(buffer, bufSize, MPI_DOUBLE, stride, TAG_INIT, MPI_COMM_WORLD); 
        //printf("initial data sent to %d!\n", stride);
      }
    }

    //printf("all data sent !\n");
  } else {
    //my rank != 0
    MPI_Status status; 
    //printf("%d waiting for data to receive\n", myRank);
    int code = MPI_Recv(points, rowCount * N, MPI_DOUBLE, 0, TAG_INIT, MPI_COMM_WORLD, &status); 
    if(code == 0) {
      //printf("%d received data no problem!\n", myRank);
    } else {
      printf("%d got an error receiving data with error code%d!\n", myRank, code);
    }
  }
  
  /* Start of computations. */
  /*
   * we swich colors black - white - black and so on
   * 0 - black 
   * 1 - white
   *
   * the actual colors on the board are as follows:
   *
   *         j
   *     01234567
   *   0 bwbwbwbw
   * i 1 wbwbwbwb 
   *   2 bwbwbwbw
   * 
   * ...and the first and last rows/columns don't change - we start calculating new values
   * starting from 1
   *
   */
  startStopwatch();
  double totalCommunicationTime = 0;
  double maxDiff, globalMaxDiff;
  numIterations = 0;

  MPI_Request sends[4];
  sends[0] = sends[1] = sends[2] = sends[3] = MPI_REQUEST_NULL; //FIXME: using only 2
  
//  double *sendBuffs[2];
//  double *recvBuffs[2];
//  sendBuffs[0] = (double*) malloc(N * sizeof(double)); //too much memory, I don't feel like math now
//  sendBuffs[1] = (double*) malloc(N * sizeof(double)); 
//  recvBuffs[0] = (double*) malloc(N * sizeof(double)); 
//  recvBuffs[1] = (double*) malloc(N * sizeof(double)); 

  do {
    int j, color;
    maxDiff = 0.0;
    for (color = 0; color < 2; ++color) {
      //skipping the first and last rows
      for (i = 1; i < rowCount - 1; ++i) {
        for (j = 1 + ((myRank * normalStrideSize + i - ((myRank != 0)?1:0)) % 2 == color ? 1 : 0); j < N - 1; j += 2) {
          //          |<-    this whole thing - just don't ask...       ->|
          double tmp, diff;
          tmp = (pointsRow(i - 1)[j] + pointsRow(i + 1)[j] + pointsRow(i)[j - 1] + pointsRow(i)[j + 1]) / 4.0;
          diff = pointsRow(i)[j];
          pointsRow(i)[j] = (1.0 - omega) * pointsRow(i)[j] + omega * tmp;
          diff = fabs(diff - pointsRow(i)[j]);
          if (diff > maxDiff)
          {
            maxDiff = diff;
          }
        }
      }

      //KISS: sending and receiving *whole rows* *here*
      struct timeval communicationStart = getTimeStructNow();
      if(myRank != 0)
        MPI_Isend(points + N,       N, MPI_DOUBLE, myRank - 1, TAG_ROW, MPI_COMM_WORLD, &sends[0]); 
      if(myRank != P - 1) {
        int off = (myRank == 0)? normalStrideSize - 1: normalStrideSize; //0th doesn't have the leading row to add to the offset
        MPI_Isend(points + N * off, N, MPI_DOUBLE, myRank + 1, TAG_ROW, MPI_COMM_WORLD, &sends[1]); 
      }

      MPI_Status status0, status1; 
      int code0, code1;
      code0 = code1 = MPI_SUCCESS;
      if(myRank != 0)
        code0 = MPI_Recv(points,                       N, MPI_DOUBLE, myRank - 1, TAG_ROW, MPI_COMM_WORLD, &status0); 
      if(myRank != P - 1) 
        code1 = MPI_Recv(points + N * (rowCount - 1) , N, MPI_DOUBLE, myRank + 1, TAG_ROW, MPI_COMM_WORLD, &status1); 
      
      if(code0 != MPI_SUCCESS || code1 != MPI_SUCCESS)
        printf("MPI_Recv from neighbors in stride no %d failed :(\n", myRank);

      if(MPI_Waitall(2, sends, MPI_STATUSES_IGNORE) != MPI_SUCCESS)
        printf("MPI_Waitall waiting for neighboring rows in stride no %d failed :(\n", myRank);
      totalCommunicationTime += getTimeSince(communicationStart);  
    }

    globalMaxDiff = getGlobalMaxDiff(maxDiff);
    ++numIterations;
  } while (globalMaxDiff > epsilon); 
  /* End of computations. */
  double duration;
  duration = stopwatchTime();
  if(myRank == 0) {
    if(DEBUG) {
      fprintf(stderr,
        "REALP, N, duration, totalCommunicationTime, numIterations, globalMaxDiff, epsilon);\n"
        "dbgout %d %d %.10f %.10f %d %.10f %.10f\n",
        REALP, N, duration, totalCommunicationTime, numIterations, globalMaxDiff, epsilon);
    } else {
      fprintf(stderr,
        "Statistics: duration(s)=%.10f #iters=%d diff=%.10f epsilon=%.10f\n",
        duration, numIterations, globalMaxDiff, epsilon);

    }
  }
  results();
  
  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize();
  return 0;
}

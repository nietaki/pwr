/* local variables and loops */

byte state = 1;
bool live[5] = 0;

proctype H(byte i) {
    printf("Hello world, I am H(%d).\n", i);
}

init {
  byte i = 1;
    do
      :: ! live[0] -> run H(0); live[0] = 1
      :: ! live[1] -> run H(1); live[1] = 1
      :: ! live[2] -> run H(2); live[2] = 1
      :: ! live[3] -> run H(3); live[3] = 1
      :: ! live[4] -> run H(4); live[4] = 1
      :: else -> break
    od
}
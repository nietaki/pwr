typedef struct {
  uint label;
  float distance;
} Neighbor;


inline float getDistance(uint trainingIdx, uint classifiedIdx, __global float* trainingCoords, float* myCoords) {
  float total = 0.0f;
  float tmp = 0.0f;
  
  for(uint i = 0; i < d; i++) {
    tmp =  trainingCoords[d * trainingIdx + i];
    tmp -= myCoords[i];
    tmp = tmp * tmp;
    total = total + tmp;
  }
  
  return total;
}

__kernel void knn_kernel( __global float *trainingCoords, __global uint *trainingLabels,
                          __global float *classifiedCoords, __global uint *classifiedLabels)
{
// uint get_work_dim()
//   liczba wymiarow uzytych w wywolaniu kernela; //niepotrzebne
// site_t get_global_size(uint D)
//   og�lna liczba work-item w wymiarze D;
// size_t get_global_id(uint D)
//   wspolrzedna globalna work-item;
// size_t get_local_size(uint D)
//   liczba work-item w danej work-group;
// size_t get_local_id(uint D)
//   wspolrzedna lokalna work-item (w ramach jednej work-group);
// size_t get_num_groups(uint D)
//   liczba work-group;
// size_t get_group_id(uint D)
  size_t gx = get_global_id(0); //classified id
//  size_t gy = get_global_id(1); 

  size_t lx = get_local_id(0); 
//  size_t ly = get_local_id(1); 
  
  size_t gxsize = get_global_size(0);
  //size_t gysize = get_global_size(1);

  size_t lxsize = get_local_size(0); 
  //size_t lysize = get_local_size(1); 
  
  __local Neighbor neighborsAll[k * groupSize];
  
  //Neighbor neighbors[k];
  __local Neighbor *neighbors = neighborsAll + (k * lx);
  uint labelCount[l];
  
  //this classified example's coordinates
  float myCoords[d];
  for(uint i = 0; i < d; i++) {
    myCoords[i] = classifiedCoords[gx * d + i];
  } 
 
  // reseting the neighbor values
  for(uint i = 0; i < k; i++) {
    neighbors[i].distance = 1000000.000;
    neighbors[i].label = 314159;
  }
  
  // resetting label counts
  for(uint i = 0; i < l; i++) {
    labelCount[i] = 0;
  }
  
  //getting the list of nearest neighbors
  for(uint i = 0; i < n; i++) {
    //for each potential neighbor i
    float dist = getDistance(i, gx, trainingCoords, myCoords);
    uint label = trainingLabels[i];
    
    float worstDistance = 0.0f;
    uint worstDistanceIdx = 0;
    for(uint j = 0; j < k; j++) {
      if(neighbors[j].distance > worstDistance) {
        worstDistance = neighbors[j].distance;
        worstDistanceIdx = j;
      }
    }
    if(worstDistance > dist) {
      neighbors[worstDistanceIdx].distance = dist;
      neighbors[worstDistanceIdx].label = label;
    }
  }
  //counting the labels
  for(uint i = 0; i < k; i++) {
    labelCount[neighbors[i].label]++;
    //labelCount[1]++;
  }
  //getting the most popular label
  uint bestCount = 0;
  uint bestLabel = 431143;
  for(uint i = 0; i < l; i++) {
    if(labelCount[i] > bestCount) {
      bestCount = labelCount[i];
      bestLabel = i;
    }
  }
  //writing back output
  classifiedLabels[gx] = bestLabel; 
  //classifiedLabels[gx] = gx; 
  
  //barrier(CLK_LOCAL_MEM_FENCE); //let's get all the values written now
  /*
   * 2 * lysize of partially sorted values are ready to be sorted
   * we're counting all the better/lesser values. We're comparing 
   * lexicographically: (distance, label, original_position)
   */
  
 
}











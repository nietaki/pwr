/*
 * matrix multiplication: 3d algorithm
 */

__kernel void matrixmul_kernel(const __global float* matrixA,
                               const __global float* matrixB,
                               __global float* matrixC,

                               const unsigned int rowA, 
                               const unsigned int colA,
                               const unsigned int colB,
                               )
{
  uint i = get_global_id(0);
  uint trow = get_global_id(1);
  uint tcol = get_global_id(2);

  uint rowC = rowA;
  uint colC = colB;
  uint matI = trow*colC + tcol;

  uint temp_size = get_local_size(0);
  __local float temp[temp_size];


  uint k = 0;
  float sum = 0;

  // compute a single multiplication
  if ((trow < rowC) && (tcol < colC) && (i < colA )) {
    temp[i] = matrixA[trow*colA + i] * matrixB[i*colB + tcol];
  }

  // parallel reduction of temp; see oclReduction for more sophisticated algorithms
  barrier(CLK_LOCAL_MEM_FENCE);  
  for (k = 1; k < temp_size; k <<= 1) {
    if (((i % (2*k)) == 0) && (i+k < temp_size))
      temp[i] += temp[i + k];
    
    barrier(CLK_LOCAL_MEM_FENCE);  
  }

  if (i == 0) {
    matrixC[matI] = temp[i];
  }


}

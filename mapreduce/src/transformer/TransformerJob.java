package transformer;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TextOutputFormat;

public class TransformerJob {
  
  public TransformerJob(long startingVertexIndex, String fromPath, String toPath) {
    super();
    this.startingVertexIndex = startingVertexIndex;
    this.fromPath = fromPath;
    this.toPath = toPath;
  }

  private long startingVertexIndex;
  private String fromPath; 
  private String toPath;
  
  public void run() throws IOException {
    Configuration conf = new Configuration();
//    conf.set("key.value.separator.in.input.line", " ");
    conf.setLong(TransformerMapper.STARTING_VERTEX_INDEX, startingVertexIndex);
    
    JobConf job = new JobConf(conf, TransformerJob.class);
    
    job.setOutputKeyClass(LongWritable.class);
    job.setOutputValueClass(Text.class);
    //no input format, using default - TextInputFormat - LongWritable -> Text, where LongWritable is the position in file
//    job.setInputFormat(KeyValueTextInputFormat.class);
    job.setOutputFormat(TextOutputFormat.class);
        
    job.setMapperClass(TransformerMapper.class);
    job.setReducerClass(TransformerReducer.class);
        
    FileInputFormat.setInputPaths(job, new Path(fromPath));
    FileOutputFormat.setOutputPath(job, new Path(toPath));
  
    job.setJobName("TransformerJob");
  
    // Run the job
    final RunningJob runningJob = JobClient.runJob(job);
    runningJob.waitForCompletion();
  }
}

package retransformer;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import bfs.Vertex;

public class RetransformerMapper extends MapReduceBase implements Mapper<LongWritable, Text, LongWritable, Text> {

  @Override
  public void map(LongWritable key, Text value,
      OutputCollector<LongWritable, Text> output, Reporter reporter)
      throws IOException {
    long id = key.get();
    Vertex v = Vertex.fromIdAndString(id, value.toString());
    
    if(v.getDistance() != Vertex.UNREACHABLE) {
      output.collect(new LongWritable(v.getDistance()), new Text(String.valueOf(id)));
    }
    
  }

}

#!/bin/bash

git fetch origin
git checkout master 
git pull origin master 

if [[ "$LD_LIBRARY_PATH" == */usr/lib64/nvidia* ]] 
then
  echo "LD_LIBRARY_PATH OK!";
else
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64/nvidia
  echo "LD_LIBRARY_PATH ammended!";
fi;

echo $LD_LIBRARY_PATH
make
./knn input16.txt

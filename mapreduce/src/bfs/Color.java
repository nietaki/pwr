package bfs;

public enum Color {
  WHITE(0),
  GRAY(1),
  BLACK(2);
  
  private int colId;
  
  Color(int i) { this.colId = i; }
  
  public int getColorId() { return this.colId; }
  
  @Override
  public String toString() {return Integer.toString(colId);}
  
  private static Color[] colors = Color.values();
  
  public static Color fromString(String in) {
    return colors[Integer.parseInt(in)];
  }
}

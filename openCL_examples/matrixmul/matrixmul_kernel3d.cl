/*
 * matrix multiplication: 3d algorithm
 */

__kernel void matrixmul_kernel(const __global float* matrixA,
                               const __global float* matrixB,
                               __global float* matrixC,
                               const unsigned int rowA, 
                               const unsigned int colA,
                               const unsigned int colB)
{
  uint i = get_global_id(0);
  uint trow = get_global_id(1);
  uint tcol = get_global_id(2);

  uint rowC = rowA;
  uint colC = colB;
  uint matI = trow*colC + tcol;
  float temp;

  if ((trow < rowC) && (tcol < colC) && (i < colA )) {
    if (i == 0)
      matrixC[matI] = 0;
    temp = matrixC[matI];
    matrixC[matI] = temp + matrixA[trow*colA + i] * matrixB[i*colB + tcol];
  }
}

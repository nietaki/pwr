package coloring;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.KeyValueTextInputFormat;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TextOutputFormat;

public class ColoringIterationJob {
  
  public ColoringIterationJob(int iterationNo, String fromPath, String toPath) {
    super();
    this.fromPath = fromPath;
    this.toPath = toPath;
    this.iterationNo = iterationNo;
  }

  private int iterationNo; //iterationNo starts from 1
  private String fromPath; 
  private String toPath;
  
  public long run() throws IOException {
    Configuration conf = new Configuration();
    
    JobConf job = new JobConf(conf, ColoringIterationJob.class);
    
    
    job.setOutputKeyClass(LongWritable.class);
    job.setOutputValueClass(Text.class);
    
    //we need the keys for Mapper
    job.setInputFormat(formats.LongTextInputFormat.class);
    job.setOutputFormat(TextOutputFormat.class);
        
    job.setMapperClass(ColoringMapper.class);
    job.setCombinerClass(ColoringCombiner.class);
    job.setReducerClass(ColoringReducer.class);
        
    FileInputFormat.setInputPaths(job, new Path(fromPath));
    FileOutputFormat.setOutputPath(job, new Path(toPath));
  
    job.setJobName("ColoringJob" + iterationNo);
  
    // Run the job
    final RunningJob runningJob = JobClient.runJob(job);
    runningJob.waitForCompletion();
    
    return runningJob.getCounters().findCounter(Counters.GRAY_VERTICES).getValue();
    
    
  }
}

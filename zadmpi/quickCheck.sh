#!/bin/bash

#mpirun -n 5 ./laplace-par.exe --verbose 42 >mpi.txt 2> /dev/null && ./laplace-seq.exe --verbose 42 >seq.txt 2>/dev/null && diff mpi.txt seq.txt

for P in $(seq 2 32)
do
  for N in $(seq 100 100 1000) 
  do
    mpirun -n $P ./laplace-par.exe --verbose $N >mpi.txt 2> /dev/null && ./laplace-seq.exe --verbose $N >seq.txt 2>/dev/null && diff mpi.txt seq.txt >/dev/null 
    #for testing purposes 
    #mpirun -n $P ./laplace-par.exe --verbose $N >mpi.txt 2> /dev/null && ./laplace-seq.exe --verbose $N >seq.txt 2>/dev/null && diff tmp.txt seq.txt >/dev/null 
    rc=$?
    if [[ $rc != 0 ]] ; then
      echo "outputs are different for P=$P and N=$N"
    fi
  done
done

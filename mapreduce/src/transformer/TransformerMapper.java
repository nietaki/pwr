package transformer;

import java.io.IOException;
import java.util.LinkedList;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.Mapper;

import bfs.Color;
import bfs.Vertex;

public class TransformerMapper extends MapReduceBase implements Mapper<LongWritable, Text, LongWritable, Text> {
  public static final String STARTING_VERTEX_INDEX = "starting.vertex.index";
  
  
  private long startingVertexIndex;
  
  @Override
  public void configure(JobConf job) {
    this.startingVertexIndex = job.getLong(STARTING_VERTEX_INDEX, 0);
  }
  
  @Override
  public void map(LongWritable key, Text value,
      OutputCollector<LongWritable, Text> output, Reporter reporter)
      throws IOException {
    
    String[] tokens = value.toString().split(" ");
    
    long id = Long.parseLong(tokens[0]);
    LinkedList<Long> neighbors = new LinkedList<Long>();
    
    for(int i = 1; i < tokens.length; i++) {
      neighbors.add(Long.parseLong(tokens[i]));
    }
//    Color color = 
    Vertex v = new Vertex();
    v.setId(id);
    v.setColor((id == this.startingVertexIndex)? Color.GRAY: Color.WHITE); 
    v.setNeighbors(neighbors);
    v.setDistance((id == this.startingVertexIndex)? 0: -1);
    
    output.collect(new LongWritable(id), new Text(v.toString()));
    
  }


}

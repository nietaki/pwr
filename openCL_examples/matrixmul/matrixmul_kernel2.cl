/*
 * matrix multiplication: naive algorithm, results cached
 */

__kernel void matrixmul_kernel(const __global float* matrixA,
                               const __global float* matrixB,
                               __global float* matrixC,
                               const unsigned int rowA, 
                               const unsigned int colA,
                               const unsigned int rowB,                               
                               const unsigned int colB)
{
  uint trow = get_global_id(0);
  uint tcol = get_global_id(1);
  uint rowC = rowA;
  uint colC = colB;
  uint matI = trow*colC + tcol;
  uint i;
  float temp = 0;
  if ((trow < rowC) && (tcol < colC)) {
    for (i = 0; i < colA; i++) {
      temp += matrixA[trow*colA + i] * matrixB[i*colB + tcol];
    }
    matrixC[matI] = temp;
  }
}

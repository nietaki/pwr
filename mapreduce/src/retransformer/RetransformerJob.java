package retransformer;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.TextOutputFormat;

public class RetransformerJob {
  
  public RetransformerJob(String fromPath, String toPath) {
    super();
    this.fromPath = fromPath;
    this.toPath = toPath;
  }

  private String fromPath; 
  private String toPath;
  
  public void run() throws IOException {
    Configuration conf = new Configuration();
    
    JobConf job = new JobConf(conf, RetransformerJob.class);
    
    job.setOutputKeyClass(LongWritable.class);
    job.setOutputValueClass(Text.class);
    
    job.setInputFormat(formats.LongTextInputFormat.class);
    job.setOutputFormat(TextOutputFormat.class);
        
    job.setMapperClass(RetransformerMapper.class);
    job.setReducerClass(RetransformerReducer.class);
        
    FileInputFormat.setInputPaths(job, new Path(fromPath));
    FileOutputFormat.setOutputPath(job, new Path(toPath));
  
    job.setJobName("RetransformerJob");
  
    // Run the job
    final RunningJob runningJob = JobClient.runJob(job);
//    runningJob.waitForCompletion();
  }
}

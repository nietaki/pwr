package coloring;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import bfs.Vertex;

public class ColoringCombiner extends MapReduceBase implements
    Reducer<LongWritable, Text, LongWritable, Text> {
  
  ColoringCombinerImpl combiner = new ColoringCombinerImpl();

  @Override
  public void reduce(LongWritable key, Iterator<Text> vertices,
      OutputCollector<LongWritable, Text> output, Reporter reporter)
      throws IOException {
    long id = key.get();
    
    Vertex ret = combiner.combine(key, vertices);
    
    output.collect(new LongWritable(id), new Text(ret.toString()));
  }

}

package transformer;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class TransformerReducer extends MapReduceBase implements
    Reducer<LongWritable, Text, LongWritable, Text> {

  @Override
  public void reduce(LongWritable key, Iterator<Text> vertices,
      OutputCollector<LongWritable, Text> output, Reporter reporter)
      throws IOException {
    int valueCount = 0;
    while(vertices.hasNext()) {
      valueCount++;
      output.collect(key, vertices.next());
    }
    if(valueCount != 1){
      //TODO: throw something or something ;
    }
    
  }

}

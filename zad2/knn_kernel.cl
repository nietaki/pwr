typedef struct {
  unsigned int label;
  float distance;
} Neighbor;

float floatRep(float distance, uint original_position) {
  return distance * 512.0 + 1.0 * original_position;
}

uint zeroIfFirstLE(float first, float second) { 
  if((first - second) <= 0.0)
    return 0;
  else
    return 1;
}


__kernel void knn_kernel(const uint n, const uint d, const uint q,
                          __global float *trainingCoords, __global uint *trainingLabels,
                          __global float *classifiedCoords, __global uint *classifiedLabels,
                          __global Neighbor *neighbors,
													__local  Neighbor *sortingArea, //twice as big as neighbors
                          uint rowBatchLine)
{
// uint get_work_dim()
//   liczba wymiarów użytych w wywołaniu kernela; //niepotrzebne
// site_t get_global_size(uint D)
//   ogólna liczba work-item w wymiarze D;
// size_t get_global_id(uint D)
//   współrzędna globalna work-item;
// size_t get_local_size(uint D)
//   liczba work-item w danej work-group;
// size_t get_local_id(uint D)
//   współrzędna lokalna work-item (w ramach jednej work-group);
// size_t get_num_groups(uint D)
//   liczba work-group;
// size_t get_group_id(uint D)

  size_t gx = get_global_id(0); //classified id
  size_t gy = rowBatchLine + get_global_id(1); //training id

  size_t lx = get_local_id(0); 
  size_t ly = get_local_id(1); 

  size_t gxsize = get_global_size(0);
  //size_t gysize = get_global_size(1);

  size_t lxsize = get_local_size(0); 
  size_t lysize = get_local_size(1); 
  
  __global Neighbor *gn;
  __local  Neighbor *ln;
  __local  Neighbor *ln2;

  uint i = 0;
  if(gx >= q)  {
    return; //there's no such classified example, we don't need this thread 
  }
  if(gy >= n) {
    //there's no such training example, but we still need this thread for sorting
    
    //BEGIN copied from below
    gn = neighbors +   (gx * lysize) + ly;
    ln = sortingArea + (lx * lysize * 2) + ly;
  
    (*ln) = *gn; //putting the already sorted at the beginning of the sorting area
    //END copied from below

    //if n is too small, we need that
    sortingArea[(lx * lysize * 2) + lysize + ly].distance = INFINITY; 
    sortingArea[(lx * lysize * 2) + lysize + ly].label = 577; 
  } else {

    /*
     * let's get pointers to the beginning n and q segments used by the work group
     * it can later be replaced by copying the memory to local memory
     */
    // TODO: copying training anc classified examples to local memory
    // Done line by line by the workers  
    
    __global float *classVec = classifiedCoords + d * gx;   
    __global float *trainVec = trainingCoords + d * gy; 
    float distance = 0;
    float diff = 0;
   
    //calculating the distance
    for(i = 0; i < d; i++) {
      diff = classVec[i] - trainVec[i];
      distance = distance + diff * diff;
    }
   
    //copying the current sorted neighbors
      
    gn = neighbors +   (gx * lysize) + ly;
    ln = sortingArea + (lx * lysize * 2) + ly;
  
    (*ln) = *gn; //putting the already sorted at the beginning of the sorting area
    //(*gn).label = gx * gy; //this is just fun and games
   
    /* 
     * storing the distance 
     */
    ln2 = sortingArea + (lx * lysize * 2) + lysize + ly;
    
    ln2->distance = distance;
    ln2->label = trainingLabels[gy]; //FIXME
    //(*gn) = (*ln2); //this is just fun and games too
  } 
  barrier(CLK_LOCAL_MEM_FENCE); //let's get all the values written now
  /*
   * 2 * lysize of partially sorted values are ready to be sorted
   * we're counting all the better/lesser values. We're comparing 
   * lexicographically: (distance, label, original_position)
   */
  
  ln = sortingArea + (lx * lysize * 2); //column beginning

  Neighbor an, bn;
  uint a_pos, b_pos;
  uint a_target_pos, b_target_pos;
  a_target_pos = 0;
  b_target_pos = 0;

  a_pos = ly;
  b_pos = lysize + ly;

  an = *(ln + a_pos);
  bn = *(ln + b_pos);
 
  float fa, fb, fcur;
  for(i = 0; i < lysize * 2; i++) {
    fcur = floatRep(ln[i].distance, i);
    fa   = floatRep(an.distance, a_pos);
    fb   = floatRep(bn.distance, b_pos);

    a_target_pos = a_target_pos + zeroIfFirstLE(fa, fcur); 
    b_target_pos = b_target_pos + zeroIfFirstLE(fb, fcur); 

  }
  //target positions calculated 
  //neighbors[(gx * lysize)].distance = 666.0; //all play
  if(a_target_pos < lysize)  {
    neighbors[(gx * lysize) + a_target_pos] = an;
    //neighbors[(gx * lysize) + a_target_pos].distance = 111.0;
    
  }
  if(b_target_pos < lysize) {
    neighbors[(gx * lysize) + b_target_pos] = bn;
    //neighbors[(gx * lysize) + b_target_pos].distance = 222.0; 
  }
}











#include "knn.hpp"

#define CREATE_BUFFER(NAME, FLAGS, SIZE) NAME##Buffer = clCreateBuffer( \
    context, \
    FLAGS, \
    SIZE, \
    NAME, \
    &status); \
if(status != CL_SUCCESS) { \
  std::cout<< "Error: clCreateBuffer (" << #NAME << ")\n"; \
  return 1; \
} 

#define SET_KERNEL_ARG(ARGNO, SIZE, ARG) status = clSetKernelArg( \
      kernel, \
      ARGNO , \
      SIZE , \
      ARG ); \
  if(status != CL_SUCCESS) { \
    std::cout<<"Error: Setting kernel argument. n"; \
    return 1;\
  }



#define RELEASE_MEM_OBJECT(BUFFER) status = clReleaseMemObject( BUFFER ); \
  if(status != CL_SUCCESS) {\
    std::cout<<"Error: In clReleaseMemObject \n"; \
    return 1; \
  }

#define MYFREE(PTR) if(PTR != NULL) { \
    free(PTR); \
    PTR = NULL; \
  }


//#define LOCAL_WORK_SIZE_Y 32
#define KERNEL_NAME "knn_kernel.cl"
//#define KERNEL_NAME "Template_Kernels.cl"

#define LOCAL_WORK_SIZE_X 32 
#define LOCAL_WORK_SIZE_Y 32 

cl_uint divideAndCeil(cl_uint x, cl_uint y) {
 return 1 + ((x-1) / y); 
}

void writeResults(char* outFileName) {
  FILE *outFile;
  outFile = fopen(outFileName, "w");
  
  for(unsigned int i = 0; i < q; i++) {
//    if(i < q-1) {
      fprintf(outFile, "%u\n", classifiedLabels[i]);
//    } else {
//      fprintf(outFile, "%u", classifiedLabels[i]);
//    }
  }
  fclose(outFile);
}

void retrieveBestNeighbors() {
  unsigned int* counts = (unsigned int*) malloc(l * sizeof(unsigned int));
  Neighbor *nb;

  unsigned int bestCount = 0;
  
  unsigned int bestLabel = 333;

  for(unsigned int i = 0; i < q; i++) {
    //resetting the values
    for(unsigned int j = 0; j < l; j++ ) {
      counts[j] = 0.0;
    }
    //counting the values
    nb = neighbors + i * LOCAL_WORK_SIZE_Y; 
    for(unsigned int j = 0; j < k; j++) {
      unsigned int label = nb[j].label;
      counts[label] = counts[label] + 1;
    }
    //getting best label
    bestCount = 0;
    bestLabel = 333;
    
    //checking the best count
    for(unsigned int j = 0; j < l; j++) {
      if (counts[j] > bestCount) { //smaller labels have preference
        bestCount = counts[j];
        bestLabel = j;
      }
    }
    
    classifiedLabels[i] = bestLabel;
  }
}

/*
 * \brief Host Initialization 
 *        Allocate and initialize memory 
 *        on the host. Print input array. 
 */
  int
readInputAndInitMemory(char* inFileName) {
 trainingLabels = NULL;
 trainingCoords = NULL;

  std::ifstream infile(inFileName);

  if (!infile) {
    std::cout<<"Error: Failed to open input file\n";
    return 1;
  }
  infile >> n >> d >> l >> q >> k;

  batchCount = divideAndCeil(n, LOCAL_WORK_SIZE_Y);
  
  rowBatchLineNos = (cl_uint*) malloc(batchCount * sizeof(cl_uint));

  //all the below will be initialized
  trainingLabels = (cl_uint*) malloc(n * sizeof(cl_uint));
  trainingCoords = (cl_float*) malloc(n * d * sizeof(cl_float));

  classifiedLabels = (cl_uint*) malloc(q * sizeof(cl_uint));
  classifiedCoords = (cl_float*) malloc(q * d * sizeof(cl_float));


  if(trainingLabels == NULL || trainingCoords == NULL) {
    std::cout<<"Error: Failed to allocate input or output memory on host\n";
    return 1; 
  }
  //training examples
  for(unsigned int row = 0; row < n; row++) {
    infile >> trainingLabels[row];
    for(unsigned int dim = 0; dim < d; dim++) {
      unsigned int offset = row * d + dim;
      infile >> trainingCoords[offset]; 
    }
  }
  //classified examples
  for(unsigned int row = 0; row < q; row++) {
    classifiedLabels[row] = 13; //initializing output labels
    for(unsigned int dim = 0; dim < d; dim++) {
      unsigned int offset = row * d + dim;
      infile >> classifiedCoords[offset]; 
    }
  }
  

//  for(unsigned int tmp = 0; tmp < q*d; tmp++) {
//    if(classifiedCoords[tmp] != 0.0) {
//      std::cout << "classified Coords != 0.0: " << classifiedCoords[tmp] << std::endl;
//    }
//  }
  infile.close( ) ;
  //print1DArray(std::string("Input").c_str(), input, width);
  return 0;
}


int createBuffers(void) {
  cl_int status; 
//  cl_mem trainingCoordsBuffer;
//  cl_mem trainingLabelsBuffer;
//  
//  cl_mem classifiedCoordsBuffer; 
//  cl_mem classifiedLabelsBuffer; 
//  
//  cl_mem neighborsBuffer;
//  CREATE_BUFFER(NAME, FLAGS, SIZE) 
  
  /* fill these two with the starting values
  Neighbor *neighbors;
  */
  
  //initialize uninitialize buffers:
	
  neighborsSize = q * LOCAL_WORK_SIZE_Y * sizeof(Neighbor);
  neighbors = (Neighbor*) malloc(neighborsSize) ;

  for(uint i = 0; i < q * LOCAL_WORK_SIZE_Y; i++) {
    neighbors[i].label = 14;
    neighbors[i].distance = 10E6;//FIXME: change to sth larger //don't want to mess with limits<cl_float>::infinity();
  }

  CREATE_BUFFER(trainingCoords, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float) * n * d)
  
  CREATE_BUFFER(trainingLabels, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint) * n)
  
  CREATE_BUFFER(classifiedCoords, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float) * q * d)
  
  CREATE_BUFFER(classifiedLabels, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_uint) * q)

  //working buffer
  CREATE_BUFFER(neighbors, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, q * LOCAL_WORK_SIZE_Y * sizeof(Neighbor) )

  return status;
}


/*
 * \brief OpenCL related initialization 
 *        Create Context, Device list, Command Queue
 *        Create OpenCL memory buffer objects
 *        Load CL file, compile, link CL source 
 *      Build program and kernel objects
 */
int
initializeCL(void) {
  cl_int status = 0;
  size_t deviceListSize;

  /*
   * Have a look at the available platforms and pick either
   * the AMD one if available or a reasonable default.
   */
  cl_uint numPlatforms;
  cl_platform_id platform = NULL;
  status = clGetPlatformIDs(0, NULL, &numPlatforms);
  if(status != CL_SUCCESS) {
    std::cout << "Error: Getting Platforms. (clGetPlatformsIDs)\n";
    return 1;
  }

  if(numPlatforms > 0) {
    cl_platform_id* platforms = new cl_platform_id[numPlatforms];
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    if(status != CL_SUCCESS) {
      std::cout << "Error: Getting Platform Ids. (clGetPlatformsIDs)\n";
      return 1;
    }

    for(unsigned int i=0; i < numPlatforms; ++i) {
      char pbuff[100];
      status = clGetPlatformInfo(
          platforms[i],
          CL_PLATFORM_VENDOR,
          sizeof(pbuff),
          pbuff,
          NULL);
      if(status != CL_SUCCESS) {
        std::cout << "Error: Getting Platform Info.(clGetPlatformInfo)\n";
        return 1;
      }
      platform = platforms[i];
      if(!strcmp(pbuff, "NVIDIA Corporation")) {
        break;
      }
    }
    delete platforms;
  }

  if(NULL == platform) {
    std::cout << "NULL platform found so Exiting Application." << std::endl;
    return 1;
  }

  /*
   * If we could find our platform, use it. Otherwise use just available platform.
   */
  cl_context_properties cps[3] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0 };

  /////////////////////////////////////////////////////////////////
  // Create an OpenCL context
  /////////////////////////////////////////////////////////////////
  context = clCreateContextFromType(cps, 
      CL_DEVICE_TYPE_GPU, 
      NULL, 
      NULL, 
      &status);
  if(status != CL_SUCCESS) {  
    std::cout<<"Error: Creating Context. (clCreateContextFromType)\n";
    return 1; 
  }

  /* First, get the size of device list data */
  status = clGetContextInfo(context, 
      CL_CONTEXT_DEVICES, 
      0, 
      NULL, 
      &deviceListSize);
  if(status != CL_SUCCESS) {  
    std::cout<<
      "Error: Getting Context Info \
      (device list size, clGetContextInfo)\n";
    return 1;
  }

  /////////////////////////////////////////////////////////////////
  // Detect OpenCL devices
  /////////////////////////////////////////////////////////////////
  devices = (cl_device_id *)malloc(deviceListSize);
  if(devices == 0) {
    std::cout<<"Error: No devices found.\n";
    return 1;
  }

  /* Now, get the device list data */
  status = clGetContextInfo(
      context, 
      CL_CONTEXT_DEVICES, 
      deviceListSize, 
      devices, 
      NULL);
  if(status != CL_SUCCESS) { 
    std::cout<<
      "Error: Getting Context Info \
      (device list, clGetContextInfo)\n";
    return 1;
  }

  /////////////////////////////////////////////////////////////////
  // Create an OpenCL command queue
  /////////////////////////////////////////////////////////////////
  commandQueue = clCreateCommandQueue(
      context, 
      devices[0], 
      0, 
      &status);
  if(status != CL_SUCCESS) { 
    std::cout<<"Creating Command Queue. (clCreateCommandQueue)\n";
    return 1;
  }
  if(createBuffers() != 0) {
    std::cout << "problem creating the buffers\n";
    return 1;
  }

  /////////////////////////////////////////////////////////////////
  // Load CL file, build CL program object, create CL kernel object
  /////////////////////////////////////////////////////////////////
  const char * filename  = KERNEL_NAME;
  std::string  sourceStr = convertToString(filename);
  const char * source    = sourceStr.c_str();
  size_t sourceSize[]    = { strlen(source) };

  program = clCreateProgramWithSource(
      context, 
      1, 
      &source,
      sourceSize,
      &status);
  if(status != CL_SUCCESS) { 
    std::cout<<
      "Error: Loading Source into cl_program \
      (clCreateProgramWithSource): " << status << "\n";
    return 1;
  }

  /* create a cl program executable for all the devices specified */
  status = clBuildProgram(program, 1, devices, NULL, NULL, NULL);
  if(status != CL_SUCCESS) { 
    std::cout<<"Error: Building Program (clBuildProgram) " << status << "\n";
		if(status == CL_BUILD_PROGRAM_FAILURE) {
      // Determine the size of the log
      size_t log_size;
      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

      // Allocate memory for the log
      char *log = (char *) malloc(log_size);

      // Get the log
      clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

      // Print the log
      printf("%s\n", log);
		}
    return 1; 
  }

  /* get a kernel object handle for a kernel with the given name */
  kernel = clCreateKernel(program, "knn_kernel", &status);
  if(status != CL_SUCCESS) {  
    std::cout<<"Error: Creating Kernel from program. (clCreateKernel) " << status << "\n";
    return 1;
  }

  return 0;
}


/*
 * \brief Run OpenCL program 
 *      
 *        Bind host variables to kernel arguments 
 *      Run the CL kernel
 */
int 
runCLKernels(void) {
  cl_int   status;
  cl_uint maxDims;
  cl_event events[3];
  size_t globalThreads[2];
  size_t localThreads[2];
  size_t maxWorkGroupSize;
  size_t maxWorkItemSizes[3];
  cl_uint addressBits;

  /**
   * Query device capabilities. Maximum 
   * work item dimensions and the maximmum
   * work item sizes
   */ 
  status = clGetDeviceInfo(
      devices[0], 
      CL_DEVICE_MAX_WORK_GROUP_SIZE,  // maximum number of work items in a work group
      sizeof(size_t), 
      (void*)&maxWorkGroupSize, 
      NULL);
  if(status != CL_SUCCESS) {  
    std::cout<<"Error: Getting Device Info. (clGetDeviceInfo)\n";
    return 1;
  }

  status = clGetDeviceInfo(
      devices[0], 
      CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, // maximum number of dimensions
      sizeof(cl_uint), 
      (void*)&maxDims, 
      NULL);
  if(status != CL_SUCCESS) {  
    std::cout<<"Error: Getting Device Info. (clGetDeviceInfo)\n";
    return 1;
  }

  status = clGetDeviceInfo(
      devices[0], 
      CL_DEVICE_MAX_WORK_ITEM_SIZES, // maximum number of work items in each dimension of a work group
      sizeof(size_t)*maxDims,
      (void*)maxWorkItemSizes,
      NULL);
  if(status != CL_SUCCESS) {  
    std::cout<<"Error: Getting Device Info. (clGetDeviceInfo)\n";
    return 1;
  }



  status = clGetDeviceInfo(
      devices[0], 
      CL_DEVICE_MAX_WORK_ITEM_SIZES, // maximum number of work items in each dimension of a work group
      sizeof(size_t)*maxDims,
      (void*)maxWorkItemSizes,
      NULL);
  if(status != CL_SUCCESS) {  
    std::cout<<"Error: Getting Device Info. (clGetDeviceInfo)\n";
    return 1;
  }


  status = clGetDeviceInfo(devices[0],
      CL_DEVICE_ADDRESS_BITS, //maximum number of work items is bounded by 2^CL_DEVICE_ADDRESS_BITS
      sizeof(cl_uint),
      &addressBits,
      NULL);
  if (status != CL_SUCCESS) {
    std::cout << "Error: Getting Device Info. (clGetDeviceInfo)" << std::endl;
    return 1;
  }

  localThreads[0]  = LOCAL_WORK_SIZE_X ;
  localThreads[1]  = LOCAL_WORK_SIZE_Y ;
  

	globalThreads[0] = divideAndCeil(q, localThreads[0])* localThreads[0];
	globalThreads[1] = localThreads[1]; //only one layer
	
  if (globalThreads[0] > ((unsigned long) 2<<addressBits)) {
    std::cout<<"Unsupported: Device does not support requested number of global work items."<<std::endl;
    return 1;
  }
  if (localThreads[0] > maxWorkGroupSize ||  // maxWorkGroupSize is the total number of threads in a work group
      localThreads[0] > maxWorkItemSizes[0] // number of threads in each dimension is also limited
     ) {
    std::cout<<"Unsupported: Device does not support requested number of work items in a work group."<<std::endl;
    return 1;
  }
  //TODO: our real kernel-specific stuff, move out  to the actual loop
  
  
  /*** Set appropriate arguments to the kernel ***/
  
  //first, the values that don't change
  cl_uint argNo = 0; 

  SET_KERNEL_ARG(argNo++, sizeof(cl_uint), (void *)&n) 
  SET_KERNEL_ARG(argNo++, sizeof(cl_uint), (void *)&d) 
  SET_KERNEL_ARG(argNo++, sizeof(cl_uint), (void *)&q) 
  
  SET_KERNEL_ARG(argNo++, sizeof(cl_mem), (void *)&trainingCoordsBuffer) 
  SET_KERNEL_ARG(argNo++, sizeof(cl_mem), (void *)&trainingLabelsBuffer) 

  SET_KERNEL_ARG(argNo++, sizeof(cl_mem), (void *)&classifiedCoordsBuffer) 
  SET_KERNEL_ARG(argNo++, sizeof(cl_mem), (void *)&classifiedLabelsBuffer) 


  SET_KERNEL_ARG(argNo++, sizeof(cl_mem), (void *)&neighborsBuffer) 
  
	SET_KERNEL_ARG(argNo++, LOCAL_WORK_SIZE_X * LOCAL_WORK_SIZE_Y * 2 * sizeof(Neighbor) , NULL) //for sorting 
   
  unsigned int totalBatchCount = divideAndCeil(n, localThreads[0]) ;
  rowBatchLineNos[0] = 0;
  for(unsigned int rowBatchNo = 0; rowBatchNo < totalBatchCount; rowBatchNo++) { //FIXME: real values here
    //let's get the current  
    if(rowBatchNo > 0)
      rowBatchLineNos[rowBatchNo] = rowBatchLineNos[rowBatchNo - 1] + LOCAL_WORK_SIZE_Y;
    //std::cout << std::endl << rowBatchLineNos[rowBatchNo] << std::endl;
    SET_KERNEL_ARG(argNo, sizeof(cl_uint), (void *)&rowBatchLineNos[rowBatchNo]) 
 
    /* 
     * Enqueue a kernel run call.
     */
    
    cl_event *ev = NULL;
    if(rowBatchNo == totalBatchCount - 1)
      ev = &(events[2]);
    else
      ev = NULL; 

    status = clEnqueueNDRangeKernel(
        commandQueue,
        kernel,
        2, // number of dimensions
        NULL,
        globalThreads,
        localThreads,
        0,
        NULL,
        ev);
    if(status != CL_SUCCESS) { 
      std::cout<<
        "Error: Enqueueing kernel onto command queue. \
        (clEnqueueNDRangeKernel) " << status << "\n" ;
      return 1;
    }    
  } 



  //FIXME: the waiting can be done in clEnqueueReadBuffer
  /* wait for the kernel call to finish execution */
  status = clWaitForEvents(1, &(events[2]));
  if(status != CL_SUCCESS) { 
    std::cout<<
      "Error: Waiting for kernel run to finish. \
      (clWaitForEvents)\n";
    return 1;
  }

  status = clReleaseEvent(events[2]);
  if(status != CL_SUCCESS) { 
    std::cout<<
      "Error: Release event object. \
      (clReleaseEvent)\n";
    return 1;
  }

  /* Enqueue readBuffer*/
  status = clEnqueueReadBuffer(
      commandQueue,
      neighborsBuffer,
      CL_TRUE,
      0,
      q * LOCAL_WORK_SIZE_Y * sizeof(Neighbor),
      neighbors,
      0,
      NULL,
      NULL);

  if(status != CL_SUCCESS) { 
    std::cout << 
      "Error: clEnqueueReadBuffer failed. \
      (clEnqueueReadBuffer)\n";

    return 1;
  }

//  Wait for the read buffer to finish execution 
//  status = clWaitForEvents(1, &events[1]);
//  if(status != CL_SUCCESS) { 
//    std::cout<<
//      "Error: Waiting for read buffer call to finish.  (clWaitForEvents)\n";
//    return 1;
//  }

//  status = clReleaseEvent(events[1]);
//  if(status != CL_SUCCESS) { 
//    std::cout<<
//      "Error: Release event object. (clReleaseEvent)\n";
//    return 1;
//  }

  return 0;
}


/*
 * \brief Release OpenCL resources (Context, Memory etc.) 
 */
int  
cleanupCL(void) {
  cl_int status;

  status = clReleaseKernel(kernel);
  if(status != CL_SUCCESS) {
    std::cout<<"Error: In clReleaseKernel \n";
    return 1; 
  }
  status = clReleaseProgram(program);

  if(status != CL_SUCCESS) {
    std::cout<<"Error: In clReleaseProgram\n";
    return 1; 
  }

  RELEASE_MEM_OBJECT(trainingCoordsBuffer)
  RELEASE_MEM_OBJECT(trainingLabelsBuffer)
  RELEASE_MEM_OBJECT(classifiedCoordsBuffer) 
  RELEASE_MEM_OBJECT(classifiedLabelsBuffer) 
  RELEASE_MEM_OBJECT(neighborsBuffer)
 
  status = clReleaseCommandQueue(commandQueue);
  if(status != CL_SUCCESS) {
    std::cout<<"Error: In clReleaseCommandQueue\n";
    return 1;
  }
  status = clReleaseContext(context);
  if(status != CL_SUCCESS) {
    std::cout<<"Error: In clReleaseContext\n";
    return 1;
  }

  return 0;
}


/* 
 * \brief Releases program's resources 
 */
  void
cleanupHost(void)
{

  MYFREE(trainingCoords)
  MYFREE(trainingLabels)
  MYFREE(classifiedCoords)
  MYFREE(classifiedLabels) 
  MYFREE(neighbors) 

}


/*
 * \brief Print no more than 256 elements of the given array.
 *
 *        Print Array name followed by elements.
 */
void print1DArray(
    const std::string arrayName, 
    const unsigned int * arrayData, 
    const unsigned int length) {
  cl_uint i;
  cl_uint numElementsToPrint = (256 < length) ? 256 : length;

  std::cout << std::endl;
  std::cout << arrayName << ":" << std::endl;
  for(i = 0; i < numElementsToPrint; ++i) {
    std::cout << arrayData[i] << " ";
  }
  std::cout << std::endl;

}

void printNeighbors () {
  for(unsigned int i = 0; i < q; i++) {
    std::cout << "ex " << i << ": ";
		Neighbor* nb = neighbors + (i * LOCAL_WORK_SIZE_Y);
    for(unsigned int kn = 0; kn < LOCAL_WORK_SIZE_Y; kn++) { //kth neighbor
			//std::cout << "neighbors_size" << neighborsSize << std::endl;
			//std::cout << "cur offset: " << (size_t)(nb - neighbors)<< std::endl ;
			std::cout << (*nb).label << ":" << (*nb).distance << "  ";
			nb++;
    }

    std::cout << std::endl;
  }
	std::cout << "neighbors printed" << std::endl;
}

void verify() {
  bool passed = true;
  if(passed == true)
    std::cout << "Passed!\n";
  else
    std::cout << "Failed!\n";
}

/*
 * Converts the contents of a file into a string - to be used to read the kernel
 */
std::string
convertToString(const char *filename)
{
	size_t size;
	char*  str;
	std::string s;

	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if(f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);

		str = new char[size+1];
		if(!str)
		{
			f.close();
			return NULL;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
	
		s = str;
		delete[] str;
		return s;
	}
	else
	{
		std::cout << "\nFile containg the kernel code(\".cl\") not found. Please copy the required file in the folder containg the executable.\n";
		exit(1);
	}
	return NULL;
}

int 
main(int argc, char * argv[]) {
  
  if(argc != 3) {
    std::cout << "need to provide input and output files";
    return 1;
  }

  // Initialize Host application 
  if(readInputAndInitMemory(argv[1])==1)
    return 1;
  
  if(k > LOCAL_WORK_SIZE_Y) {
    std::cout << "LOCAL_WORK_SIZE_Y is too small for the given k" << std::endl;
    return 1;
  }
  // Initialize OpenCL resources
  if(initializeCL()==1)
    return 1;

  // Run the CL program
  if(runCLKernels()==1)
    return 1;

  // Print output array
  //print1DArray(std::string("Output"), output, width);
  //printNeighbors();
  // Verify output
  verify();

  retrieveBestNeighbors(); 
  
  writeResults(argv[2]); 
  // Releases OpenCL resources 
  if(cleanupCL()==1)
    return 1;

  // Release host resources
  cleanupHost();

  return 0;
}

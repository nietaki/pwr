// query about some properties of a device
// modified by krzadca@mimuw.edu.pl

#include <fstream>
#include <sstream>
#include <ios>
#include <iostream>
#include <cstdlib>
#include <CL/opencl.h>


// compile with CUDA 
// g++ -I /opt/cuda-3.2/include/ -L /opt/cuda-3.2/lib64/ info.cpp -l OpenCL

using namespace std;


int main(int argc, char* argv[])
{
  cl_int err;     // error code returned from api calls
  
  cl_platform_id cpPlatform; // OpenCL platform
  cl_device_id device_id;    // compute device id
  cl_context context;        // compute context
  cl_uint num_platforms;
  size_t size;
  
  // query about the number of platforms
  err = clGetPlatformIDs(0, NULL, &num_platforms);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to find a platform! " << err << " " << endl;
    return EXIT_FAILURE;
  }
  cout << "num platforms: " << num_platforms << endl;
  
  // Connect to a compute device
  err = clGetPlatformIDs(1, &cpPlatform, NULL);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to find a platform!" << endl;
    return EXIT_FAILURE;
  }
  
  // Get a device of the appropriate type
  err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to create a device group!" << endl;
    return EXIT_FAILURE;
  }

  // query about device name
  char device_name[1000];
  err = clGetDeviceInfo(device_id,
                        CL_DEVICE_NAME,
                        1000, 
                        device_name,
                        &size);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to query!" << endl;
    return EXIT_FAILURE;
  }
  std::cout << "Device name: " << device_name << endl;

  cl_int maxComputeUnits;

  // query about parameters of the device
  err = clGetDeviceInfo(device_id,
                        CL_DEVICE_MAX_COMPUTE_UNITS,
                        sizeof(cl_uint),
                        &maxComputeUnits,
                        &size);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to query!" << endl;
    return EXIT_FAILURE;
  }
  std::cout << "Device has max compute units: " << maxComputeUnits << endl;


  // query about mem size
  cl_ulong memSize;
  err = clGetDeviceInfo(device_id,
                        CL_DEVICE_GLOBAL_MEM_SIZE,
                        sizeof(cl_ulong),
                        &memSize,
                        &size);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to query!" << endl;
    return EXIT_FAILURE;
  }
  std::cout << "Device has global memory: " << memSize << endl;

  // device address bits
  cl_uint addressBits;
  err = clGetDeviceInfo(device_id,
                        CL_DEVICE_ADDRESS_BITS,
                        sizeof(cl_uint),
                        &addressBits,
                        &size);
  if (err != CL_SUCCESS) {
    cerr << "Error: Failed to query!" << endl;
    return EXIT_FAILURE;
  }
  std::cout << "Device has address bits: " << addressBits << endl;

	cl_uint maxDims;
	err = clGetDeviceInfo(
		device_id, 
		CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, 
		sizeof(cl_uint), 
		(void*)&maxDims, 
		NULL);
    if(err != CL_SUCCESS) 
	{  
		std::cout<<"Error: Getting Device Info. (clGetDeviceInfo)\n";
		return 1;
	}
  std::cout << "Device has maxDims: " << maxDims << endl;

	size_t maxWorkItemSizes[maxDims];
	err = clGetDeviceInfo(
		device_id, 
		CL_DEVICE_MAX_WORK_ITEM_SIZES, 
		sizeof(size_t)*maxDims,
		(void*)maxWorkItemSizes,
		NULL);
    if(err != CL_SUCCESS) 
	{  
		std::cout<<"Error: Getting Device Info. (clGetDeviceInfo)\n";
		return 1;
	}
    for (unsigned int dim = 0; dim < maxDims; dim++)
      std::cout << "Device has maxWorkItemSizes[ " << dim << "]="<< maxWorkItemSizes[dim] << endl;


  // Create a compute context
  context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
  if (!context) {
    cerr << "Error: Failed to create a compute context!" << endl;
    return EXIT_FAILURE;
  }



  clReleaseContext(context);
  
  return 0;
}

#include "knn.hpp"
#include "includes/cl.hpp"

#ifdef __CPU
  #define MY_DEVICE_TYPE CL_DEVICE_TYPE_CPU
#else
  #define MY_DEVICE_TYPE CL_DEVICE_TYPE_GPU
#endif

using namespace cl; 

cl_uint WORK_GROUP_SIZE = 16;
struct timespec getCurrTime() {
  struct timespec time;
  clock_gettime(CLOCK_REALTIME, &time);
  return time;
}

struct timespec diffTime(struct timespec& start, struct timespec& end) {
  struct timespec temp;
  if ((end.tv_nsec-start.tv_nsec)<0) {
    temp.tv_sec = end.tv_sec-start.tv_sec-1;
    temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
  } else {
    temp.tv_sec = end.tv_sec-start.tv_sec;
    temp.tv_nsec = end.tv_nsec-start.tv_nsec;
  }
  return temp;
}

cl_ulong getTimeMS(struct timespec time) {
  return time.tv_sec * 1000 + time.tv_nsec / 1000000;
}
/*
 * \brief Host Initialization 
 *        Allocate and initialize memory 
 *        on the host. Print input array. 
 */
int readInputAndInitMemory(char* inFileName) {
  trainingLabels = NULL;
  trainingCoords = NULL;

  std::ifstream infile(inFileName);

  if (!infile) {
    std::cout<<"Error: Failed to open input file\n";
    return 1;
  }
  infile >> n >> d >> l >> q >> k;
  //bool isDivisible = ((n / WORK_GROUP_SIZE) * WORK_GROUP_SIZE) == n;
  //if(isDivisible) {
  //  nOversize = n;
  //} else {
  //  nOversize = ((n / WORK_GROUP_SIZE) + 1) * WORK_GROUP_SIZE;
  //}
  //all the below will be initialized
  trainingLabels = (cl_uint*) malloc(n * sizeof(cl_uint));
  trainingCoords = (cl_float*) malloc(n * d * sizeof(cl_float));

  classifiedLabels = (cl_uint*) malloc(q * sizeof(cl_uint));
  classifiedCoords = (cl_float*) malloc(q * d * sizeof(cl_float));


  if(trainingLabels == NULL || trainingCoords == NULL) {
    std::cout<<"Error: Failed to allocate input or output memory on host\n";
    return 1; 
  }
  //training examples
  for(unsigned int row = 0; row < n; row++) {
    infile >> trainingLabels[row];
    for(unsigned int dim = 0; dim < d; dim++) {
      unsigned int offset = row * d + dim;
      infile >> trainingCoords[offset]; 
    }
  }
  //classified examples
  for(unsigned int row = 0; row < q; row++) {
    classifiedLabels[row] = 13; //initializing output labels
    for(unsigned int dim = 0; dim < d; dim++) {
      unsigned int offset = row * d + dim;
      infile >> classifiedCoords[offset]; 
    }
  }
  

//  for(unsigned int tmp = 0; tmp < q*d; tmp++) {
//    if(classifiedCoords[tmp] != 0.0) {
//      std::cout << "classified Coords != 0.0: " << classifiedCoords[tmp] << std::endl;
//    }
//  }
  infile.close( ) ;
  return 0;
}

int main(int argc, char * argv[]) {
  if(argc != 3) {
    std::cout << "need to provide input and output files";
    return 1;
  }

  // Initialize Host application 
  if(readInputAndInitMemory(argv[1]) != 0)
    return 1;
  
   try { 
        // Get available platforms
        vector<Platform> platforms;
        Platform::get(&platforms);
 
        // Select the default platform and create a context using this platform and the GPU
        cl_context_properties cps[3] = { 
            CL_CONTEXT_PLATFORM, 
            (cl_context_properties)(platforms[0])(), 
            0 
        };
        Context context(MY_DEVICE_TYPE, cps);
 
        // Get a list of devices on this platform
        vector<Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
 
        // Create a command queue and use the first device
        CommandQueue queue = CommandQueue(context, devices[0]);
 
        // Read source file
        std::ifstream sourceFile("knn_kernel.c");
        std::string sourceCode(
            std::istreambuf_iterator<char>(sourceFile),
            (std::istreambuf_iterator<char>()));
        Program::Sources source(1, std::make_pair(sourceCode.c_str(), sourceCode.length()+1));
 
        // Make program of the source code in the context
        Program program = Program(context, source);
        std::stringstream ss;
        //ss << "-cl-std=CL1.1";
        ss << " -D n=" << n;
        ss << " -D d=" << d;
        ss << " -D l=" << l;
        ss << " -D q=" << q;
        ss << " -D k=" << k;
        ss << " -D groupSize=" << WORK_GROUP_SIZE;
        //printf("Options: %s\n", ss.str().c_str());
        // Build program for these specific devices
        program.build(devices, ss.str().c_str());
 
        // Make kernel
        Kernel kernel(program, "knn_kernel");
 
        //// Create memory buffers
        Buffer trainingCoordsBuffer(  context, CL_MEM_READ_ONLY, n * d * sizeof(cl_float));
        Buffer classifiedCoordsBuffer(context, CL_MEM_READ_ONLY, q * d * sizeof(cl_float));
          
        Buffer trainingLabelsBuffer(  context, CL_MEM_READ_ONLY, n * sizeof(cl_uint)); 
        Buffer classifiedLabelsBuffer(context, CL_MEM_WRITE_ONLY, q * sizeof(cl_uint)); 
        struct timespec beforeMemory = getCurrTime();
        queue.enqueueWriteBuffer(trainingCoordsBuffer,   CL_TRUE, 0, n * d * sizeof(cl_float), trainingCoords);
        queue.enqueueWriteBuffer(classifiedCoordsBuffer, CL_TRUE, 0, q * d * sizeof(cl_float), classifiedCoords);
        queue.enqueueWriteBuffer(trainingLabelsBuffer,   CL_TRUE, 0, n * sizeof(cl_uint), trainingLabels);
        
        // Set arguments to kernel
        cl_uint argNo = 0;
        //kernel.setArg(argNo++, n);
        //kernel.setArg(argNo++, d);
        //kernel.setArg(argNo++, l);
        //kernel.setArg(argNo++, q);
        //kernel.setArg(argNo++, k);
                      
        kernel.setArg(argNo++, trainingCoordsBuffer);
        kernel.setArg(argNo++, trainingLabelsBuffer);
                      
        kernel.setArg(argNo++, classifiedCoordsBuffer);
        kernel.setArg(argNo++, classifiedLabelsBuffer);
        // TODO set the ranges correctly. Think about setting the local NDRange explicitly and do rounding up and so on
        // Run the kernel on specific ND range
        NDRange global(q, WORK_GROUP_SIZE);
        NDRange local(1, WORK_GROUP_SIZE);
        struct timespec beforeKernel = getCurrTime();
        queue.enqueueNDRangeKernel(kernel, NullRange, global, local);
        queue.finish();
        struct timespec afterKernel = getCurrTime();
 
        queue.enqueueReadBuffer(classifiedLabelsBuffer, CL_TRUE, 0, q * sizeof(cl_uint), classifiedLabels);
        struct timespec afterRead = getCurrTime();
        struct timespec kernelTime = diffTime(beforeKernel, afterKernel);
        struct timespec kernelPlusMemTime = diffTime(beforeMemory, afterRead);
        std::ofstream outfile(argv[2]);

        if (!outfile) {
          std::cout<<"Error: Failed to open output file\n";
          return 1;
        }
        for(cl_uint i = 0; i < q; i ++)
             outfile << classifiedLabels[i] << std::endl; 
        
        std::cerr << getTimeMS(kernelTime) << std::endl; 
        std::cerr << getTimeMS(kernelPlusMemTime) << std::endl; 
    } catch(Error error) {
       std::cout << error.what() << "(" << error.err() << ")" << std::endl;
    }
 
    return 0;
}

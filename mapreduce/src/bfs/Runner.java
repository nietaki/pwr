package bfs;

import java.io.IOException;

import coloring.ColoringIterationJob;

import retransformer.RetransformerJob;
import transformer.TransformerJob;

public class Runner {

  /**
   * @param args
   */
  public static void main(String[] args) {
    if (args.length < 3) {
      System.err.println("Usage: starting_node_index input_dir output_dir [max_depth]");
      System.exit(1);
    }
    for(int i = 0; i < args.length; i++) {
      System.out.print(i);
      System.out.println(": " + args[i]);
    }
    
    try {
      int depth = (args.length >= 4)? Integer.parseInt(args[3]) : Integer.MAX_VALUE; 
      String tmpBaseDir = args[2] + "rep";
      
      new TransformerJob(Long.parseLong(args[0]), args[1], tmpBaseDir + 0).run();
      long remainingGray = 13;
      int i;
      for(i = 1; i <= depth && remainingGray > 0; i++) {
        ColoringIterationJob cij =  new ColoringIterationJob(i, tmpBaseDir + (i-1), tmpBaseDir + i);
        remainingGray = cij.run();
      }
      
      new RetransformerJob(tmpBaseDir + (i-1), args[2]).run();
      
    } catch(IOException e) {
      System.err.println("Error running TransformerJob" + e.getMessage());
    }

  }

}

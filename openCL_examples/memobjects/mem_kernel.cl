/*
 * just copies input to output 
 */


__kernel void memKernel(__global  unsigned int * output,
                        __global  unsigned int * input)
{
    uint tid = get_global_id(0);
    output[tid] = input[tid];
}

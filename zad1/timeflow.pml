/*
 * autor: Jacek Królikowski <jk292749@students.mimuw.edu.pl>
 * 
 */
#define N 4
#define ARRSIZE 5
#define T1 2
#define T2 3

inline printBool(b) {
  if
    :: b -> printf("true\n");
    :: else  -> printf("false\n");
  fi;
}

inline sleep(t) {
  timer[_pid] = t;
  timer[_pid] == 0;
}

#ifdef OST 
#define SENDPART order ! _pid
#else 
#define SENDPART true
#endif 

//wykonanie pilne
#define immediate(test, action, t) { \
  atomic { \
    test; \
    SENDPART;\
    if\
      :: waiting_time <= 0 -> waiting_time = 1;\
      :: else -> waiting_time = waiting_time;\
    fi;\
    timer[_pid] = t; \
    block_on_zero[_pid] = true; \
  }\
  action;\
  block_on_zero[_pid] = false; \
}


//OST - weryfikacja podpunktu 4
#ifdef OST 
chan order = [N] of {byte}
#endif

byte critical_byte = 0;

//zainicjalizowane na 0 i false
byte timer[ARRSIZE];
bool block_on_zero[ARRSIZE];
byte kto = 0;

byte waiting_time; 
//liczy czas od wykonania protokołu wstępnego do wejścia do sekcji krytycznej
//jeśli waiting_time==0, to licznik jest wyłączony, zatrzymuje się poniżej T1 + T2 + 3


/*
filozofia: przy wykonaniu "natychmiastowym" blokujemy na zerze, wpp nie
*/
active proctype clock() {
  int i;
  do
    ::
    atomic{
      for(i: 1 .. N) {
        //zmniejszamy niezerowe wartosci
        if
          :: (timer[i] > 0) -> {
            timer[i] = timer[i] - 1; 
          }
         :: else -> timer[i] = 0;
        fi;
      }
  
      for(i: 1 .. N) {
        //blokujemy, jesli timer doszedl do zera i trzeba sie zablokowac
        //kolejnosc rosnaca w indeksach na pewno nas nie boli, bo sytuacja jest symetryczna
        //a sprawdzamy na dwóch procesach o różnych indeksach
        if 
          :: (timer[i] == 0) -> {
            block_on_zero[i] == false; 
          }
          :: else -> true; 
        fi;
        if
          :: waiting_time < T1 + T2 + 3 ->  waiting_time = waiting_time + 1;
          :: else -> true;
        fi;
      }
    }
  od;
}

active [N] proctype worker() {
  byte i = _pid;
  byte recv;
  do
    :: {
      immediate(kto == 0, kto = i, T1);
      who_equals_i: sleep(T2);
      if
        :: (kto == i) -> crit: {
          /*critical section*/ 
          critical_byte = _pid; 
#ifdef OST 
          order?recv;
          assert(recv == _pid);
#endif
          waiting_time = 0;
          kto = 0;
        }
        :: else -> true;
      fi;
    }
  od;
}



ltl crits  {[](worker[1]@crit -> !worker[2]@crit)}

ltl live   {[] ( (worker[1]@who_equals_i) -> (<> worker[1]@crit) ) } 

ltl waiting {[] (waiting_time < T1 + T2 + 3)}

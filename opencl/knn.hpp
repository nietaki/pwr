
#ifndef KNN_H_
#define KNN_H_
/*
#include <CL/cl.h>
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
*/

#define __NO_STD_VECTOR // Use cl::vector instead of STL version
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>
/*** GLOBALS ***/

typedef struct {
  cl_uint label;
  cl_float distance;
} Neighbor;


/*** KNN-SPECIFIC ***/
cl_uint k; //neighbour count
cl_uint l; //class count
cl_uint d; //vector dimension count
cl_uint n; //training examples count
//cl_uint nOversize; //training examples with some space added 
cl_uint q; //classified examples count

// cl_uint *rowBatchLineNos;
// cl_uint batchCount;

/*** OPENCL-SPECIFIC ***/
cl_float *trainingCoords;
cl_uint  *trainingLabels;

cl_float *classifiedCoords; //not as in "secret", silly
cl_uint  *classifiedLabels; 

//unsigned int neighborsSize;
//Neighbor *neighbors; //closest neighbor calculating buffer
/*
// The memory buffer that is used as input/output for OpenCL kernel 
cl_mem trainingCoordsBuffer;
cl_mem trainingLabelsBuffer;

cl_mem classifiedCoordsBuffer; 
cl_mem classifiedLabelsBuffer; 

// for kernel operation
cl_mem neighborsBuffer;//closest neighbor calculating buffer

cl_context          context;
cl_device_id        *devices;
cl_command_queue    commandQueue;

cl_program program;

// This program uses only one kernel and this serves as a handle to it 
cl_kernel  kernel;
*/

#endif  /* #ifndef KNN_H_ */

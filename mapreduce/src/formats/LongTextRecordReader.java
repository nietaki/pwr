package formats;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.LineRecordReader;
import org.apache.hadoop.mapred.RecordReader;

public class LongTextRecordReader implements RecordReader<LongWritable, Text> {
  private LineRecordReader lineReader;
  private LongWritable lineKey;
  private Text lineValue;

  public LongTextRecordReader(JobConf job, FileSplit split) throws IOException {
    lineReader = new LineRecordReader(job, split);

    this.lineKey = lineReader.createKey();
    this.lineValue = lineReader.createValue();
  }
  
  @Override
  public boolean next(LongWritable key, Text value) throws IOException {
    // get the next line
    if (!lineReader.next(lineKey, lineValue)) {
      return false;
    }

    String [] pieces = lineValue.toString().split("\t");
    if (pieces.length != 2) {
      throw new IOException("Invalid record received - does not split into two pieces");
    }

    try {
      key.set(Long.parseLong(pieces[0].trim()));
      value.set(pieces[1]);
    } catch (NumberFormatException nfe) {
      throw new IOException("Error parsing long value in record");
    }
    return true;
  }
  
  @Override
  public void close() throws IOException {
    lineReader.close();
  }

  @Override
  public LongWritable createKey() {
    return new LongWritable();
  }

  @Override
  public Text createValue() {
    return new Text();
  }

  @Override
  public long getPos() throws IOException {
    return lineReader.getPos();
  }

  @Override
  public float getProgress() throws IOException {
    return lineReader.getProgress();
  }

}

typedef struct {
  uint label;
  float distance;
} Neighbor;

inline void parallelSelectionSortNeighbors(__local Neighbor* allNeighbors, __local Neighbor* sortedNeighbors) {
  size_t ly = get_local_id(1);
  uint maxIndex = k * groupSize; //this is nicely divisible
  for(uint i = ly; i < maxIndex; i += groupSize) {
    Neighbor curNeighbor = allNeighbors[i];
    uint betterCount = 0;
    
    for(uint j = 0; j < maxIndex; j++) {
      Neighbor comparedToNeighbor = allNeighbors[j];
      if(comparedToNeighbor.distance < curNeighbor.distance ||
         (comparedToNeighbor.distance == curNeighbor.distance &&
         comparedToNeighbor.label < curNeighbor.label)
      ) {
        betterCount++;
      }
    }
    
    sortedNeighbors[betterCount] = curNeighbor;
  }
}

inline float getDistance(uint trainingIdx, __global float* trainingCoords, __local float* myCoords) {
  float total = 0.0f;
  float tmp = 0.0f;
  
  for(uint i = 0; i < d; i++) {
    tmp =  trainingCoords[d * trainingIdx + i];
    tmp -= myCoords[i];
    tmp = tmp * tmp;
    total = total + tmp;
  }
  
  return total;
}

__kernel void knn_kernel( __global float *trainingCoords, __global uint *trainingLabels,
                          __global float *classifiedCoords, __global uint *classifiedLabels)
{

  size_t qid = get_global_id(0); // index of the classified example

  size_t ly = get_local_id(1); // index of thread in the group
  
  size_t gxsize = get_global_size(0);
  //size_t gysize = get_global_size(1);

  //size_t lxsize = get_local_size(0); 
  size_t lysize = get_local_size(1); 
  
  __local float myCoords[d];
  if(ly == 0) {
    for(uint i = 0; i < d; i++) {
      myCoords[i] = classifiedCoords[qid * d + i];
    }
  }
  barrier(CLK_LOCAL_MEM_FENCE); 
  //
  __local Neighbor neighborsAll[k * groupSize];
  __local Neighbor neighborsSorted[k * groupSize];
   barrier(CLK_LOCAL_MEM_FENCE);
  
  //Neighbor neighbors[k];
  __local Neighbor *neighbors = neighborsAll + (k * ly);
  //this classified example's coordinates
 
  // reseting the neighbor values
  for(uint i = 0; i < k; i++) {
    neighbors[i].distance = 10000000000.000;
    neighbors[i].label = 314159;
  }
  
  barrier(CLK_LOCAL_MEM_FENCE);
  //getting the list of nearest neighbors
  for(uint i = ly; i < n; i += lysize) {
    //for each potential neighbor i
    float dist = getDistance(i, trainingCoords, myCoords);
    uint label = trainingLabels[i];
    
    float worstDistance = -10.0f; //even closer than 0.0f
    uint worstDistanceIdx = 0;
    for(uint j = 0; j < k; j++) {
      if(neighbors[j].distance > worstDistance) {
        worstDistance = neighbors[j].distance;
        worstDistanceIdx = j;
      }
    }
    if(worstDistance > dist) {
      neighbors[worstDistanceIdx].distance = dist;
      neighbors[worstDistanceIdx].label = label;
    }
  }
  
  barrier(CLK_LOCAL_MEM_FENCE);
  parallelSelectionSortNeighbors(neighborsAll, neighborsSorted);
  barrier(CLK_LOCAL_MEM_FENCE);
  //if(qid == 3) {
  //  for(uint i = 0; i < k; i++) {
  //    classifiedLabels[i] = neighborsSorted[i].label;
  //  }
  //}
  //return; is 2, should be 0
  
  // counting the labels 
  // resetting label counts
  __local uint labelCount[l]; //TODO sprawdzic, czy to powinno byc w tej pamieci
  barrier(CLK_LOCAL_MEM_FENCE);
  if(ly == 0) {
    for(uint i = 0; i < l; i++) {
      labelCount[i] = 0;
    }
    for(uint i = 0; i < k; i++) {
      labelCount[neighborsSorted[i].label]++;
      //labelCount[1]++;
    }
    //getting the most popular label
    uint bestCount = 0;
    uint bestLabel = 431143;
    for(uint i = 0; i < l; i++) {
      if(labelCount[i] > bestCount) {
        bestCount = labelCount[i];
        bestLabel = i;
      }
    }
    //writing back output
    classifiedLabels[qid] = bestLabel; 
    //classifiedLabels[qid] = neighborsSorted[1].label; 
  }
 
  
 
}











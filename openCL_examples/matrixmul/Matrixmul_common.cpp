#include "Matrixmul.hpp"


 cl_float* matrix_A;
 cl_float* matrix_B;
 cl_float* matrix_C;

 size_t rowA, colA;
 size_t rowB, colB;
 size_t rowC, colC;

 cl_mem   dev_matrixA;
 cl_mem   dev_matrixB;
 cl_mem   dev_matrixC;

 cl_context          context;
 cl_device_id        *devices;
 cl_command_queue    commandQueue;

 cl_program program;

 cl_kernel  kernel;


cl_float* matrix_malloc(size_t row, size_t col) {
    void* matrix_ptr;
    size_t sizeBytes = row * col * sizeof(cl_float);
    matrix_ptr = malloc(sizeBytes);
	if(matrix_ptr == NULL)
	{
		std::cout<<"Error: Failed to allocate input memory on host\n";
		exit(EXIT_FAILURE);
	}
    return (cl_float*) matrix_ptr;
  }



/*
 * \brief Host Initialization 
 *        Allocate and initialize memory 
 *        on the host. Print input array. 
 */
int
initializeHost(void) {
  rowA = 1024;
  colA = 512;
  rowB = colA;
  colB = 1024;

    rowC = rowA;
    colC = colB;
    
    matrix_A = NULL;
    matrix_B = NULL;
    matrix_C = NULL;


	/////////////////////////////////////////////////////////////////
	// Allocate and initialize memory used by host 
	/////////////////////////////////////////////////////////////////
    matrix_A = matrix_malloc(rowA, colA);
    matrix_B = matrix_malloc(rowB, colB);
    matrix_C = matrix_malloc(rowC, colC);

    for(cl_uint i = 0; i < rowA * colA; i++)
      matrix_A[i] = 1;
    for(cl_uint i = 0; i < rowB * colB; i++)
      matrix_B[i] = 2;


	// print input array
    print2DArray("matrix A", matrix_A, rowA, colA);
    print2DArray("matrix B", matrix_B, rowB, colB);
	return 0;
}

/*
 * Converts the contents of a file into a string
 */
std::string
convertToString(const char *filename)
{
	size_t size;
	char*  str;
	std::string s;

	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if(f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);

		str = new char[size+1];
		if(!str)
		{
			f.close();
			return NULL;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
	
		s = str;
		delete[] str;
		return s;
	}
	else
	{
		std::cout << "\nFile containg the kernel code(\".cl\") not found. Please copy the required file in the folder containg the executable.\n";
		exit(1);
	}
	return NULL;
}

/*
 * \brief OpenCL related initialization 
 *        Create Context, Device list, Command Queue
 *        Create OpenCL memory buffer objects
 *        Load CL file, compile, link CL source 
 *		  Build program and kernel objects
 */
int
initializeCL(const char* kernel_name)
{
    cl_int status = 0;
    size_t deviceListSize;

    /*
     * Have a look at the available platforms and pick either
     * the AMD one if available or a reasonable default.
     */

    cl_uint numPlatforms;
    cl_platform_id platform = NULL;
    status = clGetPlatformIDs(0, NULL, &numPlatforms);
    if(status != CL_SUCCESS)
    {
        std::cout << "Error: Getting Platforms. (clGetPlatformsIDs)\n";
        return 1;
    }
    
    if(numPlatforms > 0)
    {
        cl_platform_id* platforms = new cl_platform_id[numPlatforms];
        status = clGetPlatformIDs(numPlatforms, platforms, NULL);
        if(status != CL_SUCCESS)
        {
            std::cout << "Error: Getting Platform Ids. (clGetPlatformsIDs)\n";
            return 1;
        }
        for(unsigned int i=0; i < numPlatforms; ++i)
        {
            char pbuff[100];
            status = clGetPlatformInfo(
                        platforms[i],
                        CL_PLATFORM_VENDOR,
                        sizeof(pbuff),
                        pbuff,
                        NULL);
            if(status != CL_SUCCESS)
            {
                std::cout << "Error: Getting Platform Info.(clGetPlatformInfo)\n";
                return 1;
            }
            platform = platforms[i];
            if(!strcmp(pbuff, "NVIDIA Corporation"))
            {
                break;
            }
        }
        delete platforms;
    }

    if(NULL == platform)
    {
        std::cout << "NULL platform found so Exiting Application." << std::endl;
        return 1;
    }

    /*
     * If we could find our platform, use it. Otherwise use just available platform.
     */
    cl_context_properties cps[3] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0 };

	/////////////////////////////////////////////////////////////////
	// Create an OpenCL context
	/////////////////////////////////////////////////////////////////
    context = clCreateContextFromType(cps, 
                                      CL_DEVICE_TYPE_GPU, 
                                      NULL, 
                                      NULL, 
                                      &status);
    if(status != CL_SUCCESS) 
	{  
		std::cout<<"Error: Creating Context. (clCreateContextFromType)\n";
		return 1; 
	}

    /* First, get the size of device list data */
    status = clGetContextInfo(context, 
                              CL_CONTEXT_DEVICES, 
                              0, 
                              NULL, 
                              &deviceListSize);
    if(status != CL_SUCCESS) 
	{  
		std::cout<<
			"Error: Getting Context Info \
		    (device list size, clGetContextInfo)\n";
		return 1;
	}

	/////////////////////////////////////////////////////////////////
	// Detect OpenCL devices
	/////////////////////////////////////////////////////////////////
    devices = (cl_device_id *)malloc(deviceListSize);
	if(devices == 0)
	{
		std::cout<<"Error: No devices found.\n";
		return 1;
	}

    /* Now, get the device list data */
    status = clGetContextInfo(
			     context, 
                 CL_CONTEXT_DEVICES, 
                 deviceListSize, 
                 devices, 
                 NULL);
    if(status != CL_SUCCESS) 
	{ 
		std::cout<<
			"Error: Getting Context Info \
		    (device list, clGetContextInfo)\n";
		return 1;
	}

	/////////////////////////////////////////////////////////////////
	// Create an OpenCL command queue
	/////////////////////////////////////////////////////////////////
    commandQueue = clCreateCommandQueue(
					   context, 
                       devices[0], 
                       CL_QUEUE_PROFILING_ENABLE, 
                       &status);
    if(status != CL_SUCCESS) 
	{ 
		std::cout<<"Creating Command Queue. (clCreateCommandQueue)\n";
		return 1;
	}

	/////////////////////////////////////////////////////////////////
	// Create OpenCL memory buffers
	/////////////////////////////////////////////////////////////////
    dev_matrixA = clCreateBuffer(
                                 context, 
                                 CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                      sizeof(cl_float) * rowA * colA,
                      matrix_A, 
                      &status);
    if(status != CL_SUCCESS) 
	{ 
		std::cout<<"Error: clCreateBuffer (dev_matrixA)\n";
		return 1;
	}
    dev_matrixB = clCreateBuffer(
                                 context, 
                                 CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                      sizeof(cl_float) * rowB * colB,
                      matrix_B, 
                      &status);
    if(status != CL_SUCCESS) 
	{ 
		std::cout<<"Error: clCreateBuffer (dev_matrixB)\n";
		return 1;
	}

    dev_matrixC = clCreateBuffer(
                                 context, 
                                 CL_MEM_READ_WRITE,
                      sizeof(cl_float) * rowC * colC,
                      NULL, 
                      &status);
    if(status != CL_SUCCESS) 
	{ 
		std::cout<<"Error: clCreateBuffer (dev_matrixC)\n";
		return 1;
	}


	/////////////////////////////////////////////////////////////////
	// Load CL file, build CL program object, create CL kernel object
	/////////////////////////////////////////////////////////////////
    std::string  sourceStr = convertToString(kernel_name);
    const char * source    = sourceStr.c_str();
    size_t sourceSize[]    = { strlen(source) };
    
    std::cout<< "source: "<<source<<std::endl;
    
    program = clCreateProgramWithSource(
			      context, 
                  1, 
                  &source,
				  sourceSize,
                  &status);
	if(status != CL_SUCCESS) 
	{ 
	  std::cout<<
			   "Error: Loading Binary into cl_program \
			   (clCreateProgramWithBinary)\n";
	  return 1;
	}

    /* create a cl program executable for all the devices specified */
    status = clBuildProgram(program, 1, devices, NULL, NULL, NULL);
    if(status != CL_SUCCESS) 
	{ 
		std::cout<<"Error: Building Program (clBuildProgram)\n";
		return 1; 
	}

    /* get a kernel object handle for a kernel with the given name */
    kernel = clCreateKernel(program, "matrixmul_kernel", &status);
    if(status != CL_SUCCESS) 
	{  
		std::cout<<"Error: Creating Kernel from program. (clCreateKernel)\n";
		return 1;
	}

	return 0;
}




/*
 * \brief Release OpenCL resources (Context, Memory etc.) 
 */
int  
cleanupCL(void)
{
    cl_int status;

    status = clReleaseKernel(kernel);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseKernel \n";
		return 1; 
	}
    status = clReleaseProgram(program);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseProgram\n";
		return 1; 
	}
    status = clReleaseMemObject(dev_matrixA);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseMemObject (dev_matrixA)\n";
		return 1; 
	}
    status = clReleaseMemObject(dev_matrixB);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseMemObject (dev_matrixB)\n";
		return 1; 
	}
    status = clReleaseMemObject(dev_matrixC);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseMemObject (dev_matrixC)\n";
		return 1; 
	}
    status = clReleaseCommandQueue(commandQueue);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseCommandQueue\n";
		return 1;
	}
    status = clReleaseContext(context);
    if(status != CL_SUCCESS)
	{
		std::cout<<"Error: In clReleaseContext\n";
		return 1;
	}

	return 0;
}


/* 
 * \brief Releases program's resources 
 */
void
cleanupHost(void)
{
    if(matrix_A != NULL)
    {
        free(matrix_A);
        matrix_A = NULL;
    }
    if(matrix_B != NULL)
    {
        free(matrix_B);
        matrix_B = NULL;
    }
    if(matrix_C != NULL)
    {
        free(matrix_C);
        matrix_C = NULL;
    }

    if(devices != NULL)
    {
        free(devices);
        devices = NULL;
    }
}


void print2DArray(
		 const std::string arrayName, 
         const cl_float* arrayData,
         const size_t row,
         const size_t col)
{
  cl_uint ri, ci, i;
    cl_uint numRowsPrint = (20 < row)? 20 : row;
    cl_uint numColsPrint = (20 < col)? 20 : col;

    std::cout << std::endl;
    std::cout << arrayName << ":" << std::endl;
    for (ri = 0; ri < numRowsPrint; ri++) {
      for (ci = 0; ci < numColsPrint; ci++) {
        i = ci + ri * col;
        std::cout << arrayData[i] << " ";
      }
      std::cout << std::endl;
    }
  
}



void verify(void)
{
    bool passed = true;
    
    size_t r,c,i;
    cl_float temp;
    
    for (r = 0; r < rowC; r++) {
      for (c = 0; c < colC; c++) {
        temp = 0;
        for (i = 0; i < colA; i++) {
          temp += matrix_A[r*colA + i] * matrix_B[i*colB + c];
        }
        if (temp != matrix_C[r*colC + c]) {
          std::cout << "failed for r: "<<r<<" c: "<<c<<" expected: "<<temp<<" found: "<<matrix_C[r*colC + c]<<std::endl;
          passed = false;
        }
      }
    }

    if(passed == true)
        std::cout << "Passed!\n";
    else
        std::cout << "Failed!\n";
}

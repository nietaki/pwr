// -*- c++ -*-

__kernel void square( __global float* input, __global float* output,
                      const unsigned int count) { 

  uint4 v1 = (uint4) (3, 5, 7, 9);
  uint4 v2 = (uint4) (1, 2, 4, 8);
    
  uint4 v3 = v1 + v2; // (4, 7, 11, 17)
  uint4 v4 = 2 * v1; // (6, 10, 14, 18)


  float2 v = (float2) (3, 7);
  int i = get_global_id(0); 
  if (i < count) {
    output[i] = input[i] * v4.y;
  }
} 

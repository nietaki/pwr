package retransformer;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class RetransformerReducer extends MapReduceBase implements
Reducer<LongWritable, Text, LongWritable, Text> {

  @Override
  public void reduce(LongWritable key, Iterator<Text> verticeIds,
      OutputCollector<LongWritable, Text> output, Reporter reporter)
      throws IOException {
    if(verticeIds.hasNext()) {
      StringBuilder buffer = new StringBuilder(verticeIds.next().toString());
      while(verticeIds.hasNext()) {
        buffer.append(" ").append(verticeIds.next().toString());
      }
      output.collect(key, new Text(buffer.toString()));
    }
    
  }

}

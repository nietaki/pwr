
#ifndef KNN_H_
#define KNN_H_

#include <CL/cl.h>
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>

/*** GLOBALS ***/

typedef struct {
  cl_uint label;
  cl_float distance;
} Neighbor;


/*** KNN-SPECIFIC ***/
cl_uint k; //neighbour count
cl_uint l; //class count
cl_uint d; //vector dimension count
cl_uint n; //training examples count
cl_uint q; //classified examples count

cl_uint *rowBatchLineNos;
cl_uint batchCount;

/*** OPENCL-SPECIFIC ***/
cl_float *trainingCoords;
cl_uint  *trainingLabels;

cl_float *classifiedCoords; //not as in "secret", silly
cl_uint  *classifiedLabels; 

unsigned int neighborsSize;
Neighbor *neighbors; //closest neighbor calculating buffer

/* The memory buffer that is used as input/output for OpenCL kernel */
cl_mem trainingCoordsBuffer;
cl_mem trainingLabelsBuffer;

cl_mem classifiedCoordsBuffer; 
cl_mem classifiedLabelsBuffer; 

// for kernel operation
cl_mem neighborsBuffer;//closest neighbor calculating buffer

cl_context          context;
cl_device_id        *devices;
cl_command_queue    commandQueue;

cl_program program;

/* This program uses only one kernel and this serves as a handle to it */
cl_kernel  kernel;


/*** FUNCTION DECLARATIONS ***/
/*
 * OpenCL related initialisations are done here.
 * Context, Device list, Command Queue are set up.
 * Calls are made to set up OpenCL memory buffers that this program uses
 * and to load the programs into memory and get kernel handles.
 */
int initializeCL(void);

/*
 *
 */
std::string convertToString(const char * filename);

/*
 * This is called once the OpenCL context, memory etc. are set up,
 * the program is loaded into memory and the kernel handles are ready.
 * 
 * It sets the values for kernels' arguments and enqueues calls to the kernels
 * on to the command queue and waits till the calls have finished execution.
 *
 * It also gets kernel start and end time if profiling is enabled.
 */
int runCLKernels(void);

/* Releases OpenCL resources (Context, Memory etc.) */
int cleanupCL(void);

/* Releases program's resources */
void cleanupHost(void);

/*
 * Prints no more than 256 elements of the given array.
 * Prints full array if length is less than 256.
 *
 * Prints Array name followed by elements.
 */
void print1DArray(
		 const std::string arrayName, 
         const unsigned int * arrayData, 
         const unsigned int length);


#endif  /* #ifndef KNN_H_ */
